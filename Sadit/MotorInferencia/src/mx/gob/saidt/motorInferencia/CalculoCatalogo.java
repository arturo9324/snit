/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia;

import mx.gob.saidt.motorInferencia.bean.OfertaDemanda;
import mx.gob.saidt.motorInferencia.bean.Registro;
import mx.gob.saidt.motorInferencia.dao.CalculaCatalogoDao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author erick-mirsha
 */
public class CalculoCatalogo {

    public ArrayList<OfertaDemanda> realizaCalculosDemanda() {
        ArrayList<OfertaDemanda> listaOD = new ArrayList<OfertaDemanda>();
//        for (int i = 0; i < 99999; i++) {
//            listaOD.add(null);
//        }

        CalculaCatalogoDao ccd = new CalculaCatalogoDao();

        List<Integer> lista = ccd.extraerDemandas();

        for (int i = 0; i < lista.size(); i++) {

            OfertaDemanda od = new OfertaDemanda(new ArrayList<Integer>(),
                    new ArrayList<Integer>(), new ArrayList<Integer>(),
                    new ArrayList<Integer>());

            //Sector economico
            od.campo1 = ccd.sumaSectorEconomicoDemanda(lista.get(i));

            //Actividad
            od.campo2 = ccd.sumaActividadDemanda(lista.get(i));

            //Area tecnologica
            od.campo3 = ccd.sumaAreaTecnologicaDemanda(lista.get(i));

            //Area cientifica
            od.campo4 = ccd.sumaAreaCientificaDemanda(lista.get(i));

            listaOD.add(od);

        }

        return listaOD;
    }
    

    public ArrayList<OfertaDemanda> realizaCalculosOferta() {
        ArrayList<OfertaDemanda> listaOD = new ArrayList<OfertaDemanda>();
//        for (int i = 0; i < 99999; i++) {
//            listaOD.add(null);
//        }

        CalculaCatalogoDao ccd = new CalculaCatalogoDao();

        List<Integer> lista = ccd.extraerOfertas();

        for (int i = 0; i < lista.size(); i++) {

            OfertaDemanda od = new OfertaDemanda(new ArrayList<Integer>(),
                    new ArrayList<Integer>(), new ArrayList<Integer>(),
                    new ArrayList<Integer>());

            //Sector economico
            od.campo1 = ccd.sumaSectorEconomicoOferta(lista.get(i));

            //Actividad
            od.campo2 = ccd.sumaActividadOferta(lista.get(i));

            //Area tecnologica
            od.campo3 = ccd.sumaAreaTecnologicaOferta(lista.get(i));

            //Area cientifica
            od.campo4 = ccd.sumaAreaCientificaOferta(lista.get(i));

            listaOD.add(od);

        }

        return listaOD;
    }
    
    public ArrayList<OfertaDemanda> realizaCalculosDemandaId() {
        ArrayList<OfertaDemanda> listaOD = new ArrayList<OfertaDemanda>();
//        for (int i = 0; i < 99999; i++) {
//            listaOD.add(null);
//        }

        CalculaCatalogoDao ccd = new CalculaCatalogoDao();

        List<Integer> lista = ccd.extraerDemandas();//estrae todas las demandas activas
        
        for (int i = 0; i < lista.size()-1; i++) {
            int idActual = lista.get(i);//obtener id actual
            int idSig = lista.get(i+1);//obtener la siguiente id
            
            int faltan = (idSig - idActual);//obtener diferencias
            //System.out.println("Faltan: "+faltan);
            
            for (int j = 1; j < faltan; j++) {
                listaOD.add(null);//poner en null las faltantes 
            }
            
        }

        for (int i = 0; i < lista.size(); i++) {

            OfertaDemanda od = new OfertaDemanda(new ArrayList<Integer>(),
                    new ArrayList<Integer>(), new ArrayList<Integer>(),
                    new ArrayList<Integer>());

            //Sector economico
            od.campo1 = ccd.sumaSectorEconomicoDemanda(lista.get(i));//obtiene la suma del sector econimico de demanda???

            //Actividad
            od.campo2 = ccd.sumaActividadDemanda(lista.get(i));

            //Area tecnologica
            od.campo3 = ccd.sumaAreaTecnologicaDemanda(lista.get(i));

            //Area cientifica
            od.campo4 = ccd.sumaAreaCientificaDemanda(lista.get(i));

            listaOD.add(od);

        }

        return listaOD;
    }
    

    public ArrayList<OfertaDemanda> realizaCalculosOfertaId() {
        ArrayList<OfertaDemanda> listaOD = new ArrayList<OfertaDemanda>();
//        for (int i = 0; i < 99999; i++) {
//            listaOD.add(null);
//        }

        CalculaCatalogoDao ccd = new CalculaCatalogoDao();

        List<Integer> lista = ccd.extraerOfertas();//obtener ofertas validas
        
        for (int i = 0; i < lista.size()-1; i++) {
            int idActual = lista.get(i);
            int idSig = lista.get(i+1);
            
            int faltan = (idSig - idActual);
            //System.out.println("Faltan: "+faltan);
            
            for (int j = 1; j < faltan; j++) {
                listaOD.add(null);
            }
            
        }

        for (int i = 0; i < lista.size(); i++) {

            OfertaDemanda od = new OfertaDemanda(new ArrayList<Integer>(),
                    new ArrayList<Integer>(), new ArrayList<Integer>(),
                    new ArrayList<Integer>());

            //Sector economico
            od.campo1 = ccd.sumaSectorEconomicoOferta(lista.get(i));//obtener sumas ????

            //Actividad
            od.campo2 = ccd.sumaActividadOferta(lista.get(i));

            //Area tecnologica
            od.campo3 = ccd.sumaAreaTecnologicaOferta(lista.get(i));

            //Area cientifica
            od.campo4 = ccd.sumaAreaCientificaOferta(lista.get(i));

            listaOD.add(od);

        }

        return listaOD;
    }

    public double calculoRuteo(int profundidad) {
        double calculo = 0.0;

        calculo = (1.0 + 1.0 / (Math.pow(2.0, (double) (profundidad + 1))));

        return calculo;
    }

    int niveles = 0;

    public ArrayList<Registro> calculosCatalogo(String catalogo, int niv) {
        niveles = niv;
        CalculaCatalogoDao ccd = new CalculaCatalogoDao();

        ccd.suma(catalogo);// se realiza la suma de los registros de cada nivel deacuerdo al catalogo

        ArrayList<Registro> lista = new ArrayList<Registro>();

        List<Registro> lista0 = new ArrayList<Registro>();

        lista0 = ccd.extraeCatalogo(0, 0, catalogo);

        List<Registro> listan1 = new ArrayList<Registro>();
        List<Registro> listan2 = new ArrayList<Registro>();
        List<Registro> listan3 = new ArrayList<Registro>();
        List<Registro> listan4 = new ArrayList<Registro>();

        List<Registro> auxn0 = new ArrayList<Registro>();
        List<Registro> auxn1 = new ArrayList<Registro>();
        List<Registro> auxn2 = new ArrayList<Registro>();
        List<Registro> auxn3 = new ArrayList<Registro>();
        List<Registro> auxn4 = new ArrayList<Registro>();

        ccd.conecta();

        for (int i = 0; i < lista0.size(); i++) {
            Registro r0 = lista0.get(i);
            ArrayList<String> rhr0 = new ArrayList<String>();
            rhr0.add(r0.nodo);
            ArrayList<String> pr0 = new ArrayList<String>();
            pr0.add("0.0");//peso de la distancia de la ruta

            r0.ruta_hacia_raiz = rhr0;
            r0.pesos_en_ruta = pr0;

            auxn0.add(r0);

            if (niveles >= 1) {
                listan1 = ccd.extraeCatalogo(1, 0, catalogo);//obtinen todas las actividades economicas de nivel 1
                //de todas las que se obtubieron se hace un for y se recorren una por una

                for (int j = 0; j < listan1.size(); j++) {

                    Registro r1 = listan1.get(j);
                    ArrayList<String> rhr1 = new ArrayList<String>();
                    rhr1.add(r1.nodo);
                    rhr1.addAll(rhr0);
                    ArrayList<String> pr1 = new ArrayList<String>();

//                for (int k = 0; k < 2; k++) {
//                    pr1.add(calculoRuteo(k) + "");
//                }
                    pr1.add("0.0");
                    pr1.add("2.0");

                    r1.ruta_hacia_raiz = rhr1;
                    r1.pesos_en_ruta = pr1;

                    auxn1.add(r1);//agregar elementos de nivel 1

                    if (niveles >= 2) {
                        listan2 = ccd.extraeCatalogo(2, r1.idBd, catalogo);

                        for (int k = 0; k < listan2.size(); k++) {
                            Registro r2 = listan2.get(k);
                            ArrayList<String> rhr2 = new ArrayList<String>();
                            rhr2.add(r2.nodo);
                            rhr2.addAll(rhr1);
                            ArrayList<String> pr2 = new ArrayList<String>();

                            pr2.add("0.0");
                            pr2.add(calculoRuteo(0) + "");
                            pr2.add("2.0");

                            r2.ruta_hacia_raiz = rhr2;
                            r2.pesos_en_ruta = pr2;

                            auxn2.add(r2);

                            if (niveles >= 3) {
                                listan3 = ccd.extraeCatalogo(3, r2.idBd, catalogo);

                                for (int l = 0; l < listan3.size(); l++) {
                                    Registro r3 = listan3.get(l);
                                    ArrayList<String> rhr3 = new ArrayList<String>();
                                    rhr3.add(r3.nodo);
                                    rhr3.addAll(rhr2);
                                    ArrayList<String> pr3 = new ArrayList<String>();

                                    pr3.add("0.0");
                                    for (int m = 2; m > 0; m--) {
                                        pr3.add(calculoRuteo(m - 1) + "");
                                    }
                                    pr3.add("2.0");

                                    r3.ruta_hacia_raiz = rhr3;
                                    r3.pesos_en_ruta = pr3;

                                    auxn3.add(r3);

                                    if (niveles == 4) {
                                        listan4 = ccd.extraeCatalogo(4, r3.idBd, catalogo);

                                        for (int m = 0; m < listan4.size(); m++) {
                                            Registro r4 = listan4.get(m);
                                            ArrayList<String> rhr4 = new ArrayList<String>();
                                            rhr4.add(r4.nodo);
                                            rhr4.addAll(rhr3);
                                            ArrayList<String> pr4 = new ArrayList<String>();

                                            pr4.add("0.0");
                                            for (int n = 3; n > 0; n--) {
                                                pr4.add(calculoRuteo(n - 1) + "");
                                            }
                                            pr4.add("2.0");

                                            r4.ruta_hacia_raiz = rhr4;
                                            r4.pesos_en_ruta = pr4;

                                            auxn4.add(r4);

                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }

        }

        lista.addAll(auxn0);
        lista.addAll(auxn1);

        lista.addAll(auxn2);
        lista.addAll(auxn3);
        lista.addAll(auxn4);
        ccd.desconecta();

//        System.out.println("******AUX0********");
//        mostrarTabla(auxn0);
//
//        System.out.println("******AUX1********");
//        mostrarTabla(auxn1);
//
//        System.out.println("******AUX2********");
//        mostrarTabla(auxn2);
//
//        System.out.println("******AUX3********");
//        mostrarTabla(auxn3);
//
//        System.out.println("******AUX4********");
//        mostrarTabla(auxn4);
//
//        System.out.println("*******FINAL**********");
//        mostrarTabla(lista);
        lista = ordenarTabla(auxn0, auxn1, auxn2, auxn3, auxn4);

//        System.out.println("*********ORDEN************");
        //mostrarTabla(lista);

        return lista;

    }

    public ArrayList<Registro> ordenarTabla(List<Registro> auxn0, List<Registro> auxn1, List<Registro> auxn2, List<Registro> auxn3, List<Registro> auxn4) { //recibe todas las listas por separado
        ArrayList<Registro> lista = new ArrayList<Registro>();

        for (int i = 0; i < auxn0.size(); i++) {
            lista.add(auxn0.get(i));
        }

        for (int i = 0; i < auxn1.size(); i++) {
            lista.add(auxn1.get(i));
        }

        for (int i = 0; i < auxn1.size(); i++) {
            List<Registro> hijos2 = obtenerHijos(auxn2, auxn1.get(i).ruta_hacia_raiz.get(0), 1);
            lista.addAll(hijos2);

            for (int j = 0; j < hijos2.size(); j++) {
                List<Registro> hijos3 = obtenerHijos(auxn3, hijos2.get(j).ruta_hacia_raiz.get(0), 1);
                lista.addAll(hijos3);

                for (int k = 0; k < hijos3.size(); k++) {
                    List<Registro> hijos4 = obtenerHijos(auxn4, hijos3.get(k).ruta_hacia_raiz.get(0), 1);
                    lista.addAll(hijos4);
                }
            }
        }

        return lista;
    }

    public List<Registro> obtenerHijos(List<Registro> hijos, String padre, int posHijo) {
        List<Registro> lista = new ArrayList<Registro>();
        for (int i = 0; i < hijos.size(); i++) {
            if (hijos.get(i).ruta_hacia_raiz.get(posHijo).equals(padre)) {
                lista.add(hijos.get(i));
            }
        }
        return lista;
    }

    public void mostrarTabla(List<Registro> tabla) {
        System.out.println("*********************");
        for (int i = 0; i < tabla.size(); i++) {
            Registro r = tabla.get(i);

            System.out.println("Nodo: " + r.nodo);
            System.out.println("RutaRaiz: " + r.ruta_hacia_raiz);
            System.out.println("PesosRuta: " + r.pesos_en_ruta);
            System.out.println("-----------------------------");
        }
        System.out.println("*********************");
    }

    public static void mainX(String args[]) {
        CalculoCatalogo cc = new CalculoCatalogo();

        ArrayList<OfertaDemanda> demandas = cc.realizaCalculosDemanda();

        System.out.println("Demandas extraidas: " + demandas.size());

        for (int i = 0; i < demandas.size(); i++) {
            OfertaDemanda od = demandas.get(i);
            System.out.println("Campo 1 (ActividadEconomica): " + od.campo1);
            System.out.println("Campo 2 (Actividad): " + od.campo2);
            System.out.println("Campo 3 (Area Tecnologica): " + od.campo3);
            System.out.println("Campo 4 (Area Cientifica): " + od.campo4);
            System.out.println("Valor: " + od.valorAcumulado);
            System.out.println("-------------------------------");
        }

        List<OfertaDemanda> ofertas = cc.realizaCalculosDemanda();

        System.out.println("Ofertas extraidas: " + ofertas.size());

        for (int i = 0; i < ofertas.size(); i++) {
            OfertaDemanda od = ofertas.get(i);
            System.out.println("Campo 1 (ActividadEconomica): " + od.campo1);
            System.out.println("Campo 2 (Actividad): " + od.campo2);
            System.out.println("Campo 3 (Area Tecnologica): " + od.campo3);
            System.out.println("Campo 4 (Area Cientifica): " + od.campo4);
            System.out.println("Valor: " + od.valorAcumulado);
            System.out.println("----------------------");
        }
    }
}
