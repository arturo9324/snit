/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia;

import mx.gob.saidt.motorInferencia.dao.DaoFiltrados;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author
 */
public class FiltradoRecursosAsociados {

    static final float MAXIMA_DISTANCIA = 10000.0f;
    static List matrizRecurso = new ArrayList();
    static List matrizInfraestructura = new ArrayList();
    static List matrizObjetoConocimiento = new ArrayList();

    static List demandasRecursosAsociados(List matrizTriangular, String recursoAsociado, List demandasOfertas) {
//        List demandasOfertas = new ArrayList();
        DaoFiltrados datos = new DaoFiltrados();

        List deof = null;// se almacena la lsita de una demnada con sus ofertas
        int de = 0;//almacena la demnada
        int pesorecurso = 0;//sumatorio del peso con la similitud
        String ofde = "";//empatar oferta y demanda para burcarla en la matriz
        String similitudRecurso = "";
        String[] sr = null;
        String buscar = "";
        double sumaPeso = 0.0;
        List demandasOfertasRecursoAsociados = new ArrayList();

        //recorrer lista demandasOfertas
        for (int i = 0; i < demandasOfertas.size(); i++) {

            LinkedList aux = new LinkedList();//almacena lista de ofertas, pesos con su demanda
            deof = (List) demandasOfertas.get(i);
            de = Integer.parseInt(deof.get(0) + "");
            //se obtiene la demnada y se consultan sus recursos
            //	System.out.println("________Demanda: "+de+" _______ ");

            List ofertas = new ArrayList();

            try {
                ofertas = (List) deof.get(1);
                //System.out.println("Demanda recursos asociados, ofertas: "+ofertas.size());

                if (ofertas.size() > 0) {

                    for (int j = 0; j < ofertas.size(); j++) {

                        List recursode = null;
                        List ofertaPeso = (List) ofertas.get(j);
                    //	System.out.println("--Oferta : "+ofertaPeso.get(0));
                        //List recursode=(List)demanda.get(1);//se obtiene la lista de recursos por demnada Estatico
                        if (recursoAsociado == "recursoTecnologico") {
                            recursode = datos.recursoDemanda(de);//se obtiene la lista de recursos por demnada
                        }
                        if (recursoAsociado == "infraestructura") {
                            recursode = datos.infraestructuraDemanda(de);
                        }
//                if (recursoAsociado == "ObjetoConocimiento") {
//                    recursode = datos.infraestructuraDemanda(de);
//                }

                        for (int rd = 0; rd < recursode.size(); rd++) {//se recorre para obtener recurso por recurso

                            List recursoOf = null;
                            int recursoD = Integer.parseInt(recursode.get(rd) + "");//se almacena el recurso
//                   		System.out.println("--recursoD: "+recursoD);
                            //List recursoOf=(List) ofertas.get(1);//se almacena lista de recursos Estatica
                            if (recursoAsociado == "recursoTecnologico") {
                                recursoOf = datos.recursoOferta(Integer.parseInt(ofertaPeso.get(0) + ""));
                            }
                            if (recursoAsociado == "infraestructura") {
                                recursoOf = datos.InfraestructuraOferta(Integer.parseInt(ofertaPeso.get(0) + ""));
                            }
//                    if (recursoAsociado == "ObjetoConocimiento") {
//                        recursoOf = datos.InfraestructuraOferta(Integer.parseInt(ofertaPeso.get(0) + ""));
//                    }
                            for (int ro = 0; ro < recursoOf.size(); ro++) {//se recorre lista para obtener recurso por recurso

                                int recursoO = Integer.parseInt(recursoOf.get(ro) + "");//se almacena el recurso
//                      				System.out.print("...recursoO: "+recursoO+"  (");
                                //obtener por el recursos de la demanda y oferta  su similitud y se busca en la matriz
                                for (int mr = 0; mr < matrizTriangular.size(); mr++) {//se recorre lista para obtener la similitid de recursos.

                                    similitudRecurso = (String) matrizTriangular.get(mr);// se almacena similitud con sus recursos

                                    sr = similitudRecurso.split("\\(");//separar similitud de sus recursos
                                    // [0]similitud y [1] recursos
                                    if (recursoD > recursoO) {
                                        buscar = recursoO + "," + recursoD + ")";
                                    } else {
                                        buscar = recursoD + "," + recursoO + ")";// se concatenen los recursos de demanda y oferta
                                    }

                                    if (sr[1].equals(buscar)) {
//                              					System.out.println(sr[0]+": ("+buscar);
                                        sumaPeso += Double.parseDouble(sr[0]);
                                        break;
                                    }

                                }

                            }

                        }

//                System.out.println("Suma por oferta: "+sumaPeso);
                        sumaPeso = sumaPeso + Double.parseDouble(ofertaPeso.get(1) + "");
                        ofertaPeso.set(1, sumaPeso);
//                 		System.out.println("*Suma por oferta: "+sumaPeso);
                        sumaPeso = 0.0;
                        aux.add(ofertaPeso);
                    }
                } else {
                    //aux.add(ofertas);
                }
            } catch (Exception ex) {
                ofertas = new ArrayList();
                //aux.add(ofertas);
                ex.printStackTrace();
            }
            aux.addFirst(de);
            //System.out.println("Final: "+aux);
            demandasOfertasRecursoAsociados.add(aux);

        }

        return demandasOfertasRecursoAsociados;
    }

    public List ordenarId(List idDemanda) {

        List id = new ArrayList();//id de las demandas ordenados con su oferta y su peso
        List deofpe = new ArrayList();;

        for (int i = 0; i < idDemanda.size(); i++) {
            List ids = (List) idDemanda.get(i);
            List aux = new ArrayList();
            List of = new ArrayList();//almacena las ofertas de la demanda
            List peso = new ArrayList();//almacena los pesos de las ofertas por demandas.

            aux.add(ids.get(0));
            for (int j = 1; j < ids.size(); j++) {
                if (j % 2 == 0) {
                    peso.add(ids.get(j));

                } else {
                    of.add(ids.get(j));
                }
            }
            aux.add(of);
            aux.add(peso);
            id.add(aux);
        }
        for (int i = 0; i < id.size(); i++) {
            List aux = new ArrayList();
            List de = (List) id.get(i);
            List of = (List) de.get(1);
            List peso = (List) de.get(2);
            aux.add(de.get(0));

            for (int j = 0; j < of.size(); j++) {
                List aux2 = new ArrayList();
                aux2.add(of.get(j));
                aux2.add(peso.get(j));

                aux.add(aux2);
            }
            deofpe.add(aux);
        }
        return deofpe;
    }

    public List recursosAsociados(List recursoTecnologico, List infraestructura) {
        List recursosAsociados = new ArrayList();

        for (int i = 0; i < recursoTecnologico.size(); i++) {
            List aux = new ArrayList();

            List demandaOfertasRecurso = (List) recursoTecnologico.get(i);
            List demandaOfertasInfraestructura = (List) infraestructura.get(i);

            aux.add(demandaOfertasRecurso.get(0));

            for (int j = 1; j < demandaOfertasRecurso.size(); ++j) {
                List ofertaPesoR = (List) demandaOfertasRecurso.get(j);
                List ofertaPesoO = (List) demandaOfertasInfraestructura.get(j);
                if (ofertaPesoR.size() > 0) {
                    // float sumaPeso=Float.parseFloat(ofertaPesoR.get(1)+"")+ Float.parseFloat(ofertaPesoO.get(1)+"");
                    ofertaPesoR.set(1, Float.parseFloat(ofertaPesoR.get(1) + "") + Float.parseFloat(ofertaPesoO.get(1) + ""));
                    aux.add(ofertaPesoR);
                } else {
                    aux.add(ofertaPesoR);
                }

            }

            recursosAsociados.add(aux);

        }
        return recursosAsociados;

    }

}
