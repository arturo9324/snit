/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia;

//import com.hp.hpl.jena.ontology.OntModel;
//import com.hp.hpl.jena.query.Query;
//import com.hp.hpl.jena.query.QueryExecution;
//import com.hp.hpl.jena.query.QueryExecutionFactory;
//import com.hp.hpl.jena.query.QueryFactory;
//import com.hp.hpl.jena.query.QuerySolution;
import mx.gob.saidt.motorInferencia.bean.OfertaDemanda;
import mx.gob.saidt.motorInferencia.bean.Registro;
import mx.gob.saidt.motorInferencia.dao.DaoFiltrados;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author
 */
public class FiltradoClasificacion {

    static final float MAXIMA_DISTANCIA = 10000.0f;

    /**
     * MÃƒÂ©todo para satisfacer las demandas mediante comparaciÃƒÂ³n con
     * ofertas
     *
     * @param nombreArchivoEntrada String
     * @param nombreArchivoSalida String
     * @param ofertas ArrayList<OfertaDemanda>
     * @param demandas ArrayList<OfertaDemanda>
     * @param matrizTriangular1 float[]
     * @param matrizTriangular2 float[]
     * @param matrizTriangular3 float[]
     * @param matrizTriangular4 float[]
     * @param n1 int
     * @param n2 int
     * @param n3 int
     * @param n4 int
     * @throws IOException
     * @throws SQLException
     */
    public List satisfacerDemandas(
            String nombreArchivoSalida, ArrayList<OfertaDemanda> ofertas,
            ArrayList<OfertaDemanda> demandas, float matrizTriangular1[],
            float matrizTriangular2[], float matrizTriangular3[],
            float matrizTriangular4[], int n1, int n2, int n3, int n4)
            throws IOException, SQLException {
        //System.out.println("entro a satisfacer demandas");

        List ofertasDemandas = new ArrayList();
        DaoFiltrados datos = new DaoFiltrados();// objeto de la clase SQL para ocupar los metodos
        // que ejecutan las sentencias

        Writer out = new OutputStreamWriter(new FileOutputStream(
                nombreArchivoSalida));
        //Scanner scanner = new Scanner(new FileInputStream(nombreArchivoEntrada));
        // Leer cada lÃƒÂ­nea del archivo de entrada
        try {
            DaoFiltrados df = new DaoFiltrados();
            //System.out.println(" Entro al try");
            float UMBRAL = 0.0f;
            float similaridad_actual = 0.0f;
            float simCampo1 = 0.0f;
            float simCampo2 = 0.0f;
            float simCampo3 = 0.0f;
            float simCampo4 = 0.0f;
            int indice = 0;
            //int n = 60;
            int n = ofertas.size();
            //		int indiceDemanda;// idDemanda
            //	int ncampos;// nfactores
            int campos_activos[] = new int[4];
            //int campo;// factor
            //	float proporcion1 = 0, proporcion2 = 0, proporcion3 = 0, proporcion4 = 0;// peso1,peso2,peso3,peso4
            long inicio;
            /*
             * // leer umbral if (scanner.hasNext()) UMBRAL =
             * Float.parseFloat(scanner.next()); System.out.println("UMBRAL: " +
             * UMBRAL);
             */

            // codigo agregado

            UMBRAL = df.obtenerUmbralClasificacion();
            System.out.println("Umbral de clasificacion: "+UMBRAL);
            
            out.write("Umbral General: "+UMBRAL+"\n\n\n");

            // 1.- almacenar los id de demandas a satisfacer
            List demanda = datos.idDemanda();//CON DEMANDA 
            // System.out.println("lista demanda base"+demanda);

            int idDemanda = 0;
            int nfactores;
            int factor = 0;
            float peso = 0;
            List factoresPeso;
            float peso1 = 0, peso2 = 0, peso3 = 0, peso4 = 0;

            for (int id = 0; id < demanda.size(); id++) {
                //System.out.println("entro a for demandas");

                // se obtiene id de la demanda en especifico
                idDemanda = Integer.parseInt(demanda.get(id) + "");
//                System.out.println("idDemanda: " + idDemanda);
                idDemanda -= 1;

                // 2.- se obtiene los nfactores a considerar por demnada
                nfactores = datos.nFactores(idDemanda + 1);
//                System.out.println("nFactores: " + nfactores);
                // inicialmente no considerar todos los campos (cero)
                for (int cc = 0; cc < 4; cc++) {
                    campos_activos[cc] = 0;
                }

                // System.out.println("1) Sector econÃƒÂ³mico\n2)Actividad\n3)Ãƒï¿½rea TecnolÃƒÂ³gica\n4)Ãƒï¿½rea CientÃƒÂ­fica");
                // se almacenar los factores de cada demanda en 1;
                // 3.- se obtiene todos los factores a considerar
                factoresPeso = datos.factoresPeso(idDemanda + 1);//se regresan los factores con su peso a considerar

                for (int ci = 0; ci < factoresPeso.size(); ci++) {
                    List datosFP = (List) factoresPeso.get(ci);//lista factor junto a su peso

                    for (int pf = 0; pf < datosFP.size(); pf++) {
                        // obtener el factor por factor
                        factor = Integer.parseInt(datosFP.get(0) + "");

                        // 4.- se obtiene el peso por cada factor.
                        //se almacena el factor 1 en la posición del factor momentaneamente
                        campos_activos[factor - 1] = 1;
                        peso = Float.parseFloat(datosFP.get(1).toString());
                          //verifica las id's de los factores 
                        if (factor == 1) {
                            //System.out.println("entro f1");
                            peso1 = Float.parseFloat(datosFP.get(1).toString());

                        }
                        if (factor == 2) {
                            //  System.out.println("entro f2");
                            peso2 = Float.parseFloat(datosFP.get(1).toString());
                        }
                        if (factor == 3) {
                            //  System.out.println("entro f3");
                            peso3 = Float.parseFloat(datosFP.get(1).toString());

                        }
                        if (factor == 4) {
                            //System.out.println("entro f4");
                            peso4 = Float.parseFloat(datosFP.get(1).toString());

                        }

                    }
//                    System.out.println("FACTOR: " + factor);
//                    System.out.println("PESO: " + peso);
                }

                // fin de codigo agregado
                // Comenzar a contar tiempo de bÃƒÂºsqueda para satisfacer la
                // demanda actual
                inicio = System.currentTimeMillis();
                // Busqueda de la oferta mas similar
                indice = 0;
                while (indice < n) {

                    // Para cada subcampo de la demanda obtener similaridad con
                    // subcampos de la oferta actual (si es que existen y si
                    // fueron activados para la bÃƒÂºsqueda)
                    //si los campos están activos se pone la similaridad nueva
                    if (campos_activos[0] == 1) {
                        simCampo1 = 0.0f;
                        if (demandas.get(idDemanda).campo1 != null
                                && ofertas.get(indice).campo1 != null) {
                            // System.out.println("Prueba: " + demandas.get(idDemanda).campo1);
                        }
                        //System.out.println("Prueba2: " + ofertas.get(indice).campo1);
                        for (int c1d = 0; c1d < demandas.get(idDemanda).campo1.size(); c1d++) {
                            if (c1d < ofertas.get(indice).campo1.size()) {
                                //todos los campos 1 de las ofertas y demandas se recorren
                                if (demandas.get(idDemanda).campo1.get(c1d) >= ofertas.get(indice).campo1.get(c1d)) {
                                    simCampo1 += matrizTriangular1[ofertas.get(indice).campo1.get(c1d)
                                            * (n1 - ofertas.get(indice).campo1.get(c1d))
                                            + demandas.get(idDemanda).campo1.get(c1d)];
                                } else {
                                    simCampo1 += matrizTriangular1[demandas.get(idDemanda).campo1.get(c1d)
                                            * (n1 - demandas.get(idDemanda).campo1.get(c1d))
                                            + ofertas.get(indice).campo1.get(c1d)];
                                }
                                //System.out.println("------simcampo1: " + simCampo1);
                            }
                        }
                    }
                    if (campos_activos[1] == 1) {
                        simCampo2 = 0.0f;
                        if (demandas.get(idDemanda).campo2 != null
                                && ofertas.get(indice).campo2 != null) //System.out.println("Prueba: "+demandas.get(idDemanda));
                        //System.out.println("Prueba2: "+ofertas.get(indice));
                        {
                            for (int c2d = 0; c2d < demandas.get(idDemanda).campo2.size(); c2d++) {
                                if (c2d < ofertas.get(indice).campo2.size()) {
                                    if (demandas.get(idDemanda).campo2.get(c2d) >= ofertas.get(indice).campo2.get(c2d)) {
                                        simCampo2 += matrizTriangular2[ofertas.get(indice).campo2.get(c2d)
                                                * (n2 - ofertas.get(indice).campo2.get(c2d))
                                                + demandas.get(idDemanda).campo2.get(c2d)];
                                    } else {
                                        simCampo2 += matrizTriangular2[demandas.get(idDemanda).campo2.get(c2d)
                                                * (n2 - demandas.get(idDemanda).campo2.get(c2d))
                                                + ofertas.get(indice).campo2.get(c2d)];
                                    }
                                    // System.out.println("------simcampo2: "+simCampo2);
                                }

                            }
                        }
                    }
                    if (campos_activos[2] == 1) {
                        simCampo3 = 0.0f;
                        if (demandas.get(idDemanda).campo3 != null
                                && ofertas.get(indice).campo3 != null) {
                            for (int c3d = 0; c3d < demandas.get(idDemanda).campo3.size(); c3d++) {
                                if (c3d < ofertas.get(indice).campo3.size()) {
                                    if (demandas.get(idDemanda).campo3.get(c3d) >= ofertas.get(indice).campo3.get(c3d)) {
                                        simCampo3 += matrizTriangular3[ofertas.get(indice).campo3.get(c3d)
                                                * (n3 - ofertas.get(indice).campo3.get(c3d))
                                                + demandas.get(idDemanda).campo3.get(c3d)];
                                    } else {
                                        simCampo3 += matrizTriangular3[demandas.get(idDemanda).campo3.get(c3d)
                                                * (n3 - demandas.get(idDemanda).campo3.get(c3d))
                                                + ofertas.get(indice).campo3.get(c3d)];
                                    }
                                }

                            }
                        }
                    }
                    if (campos_activos[3] == 1) {
                        simCampo4 = 0.0f;
                        if (demandas.get(idDemanda).campo4 != null
                                && ofertas.get(indice).campo4 != null) {
                            for (int c4d = 0; c4d < demandas.get(idDemanda).campo4.size(); c4d++) {
                                if (c4d < ofertas.get(indice).campo4.size()) {
                                    if (demandas.get(idDemanda).campo4.get(c4d) >= ofertas.get(indice).campo4.get(c4d)) //System.out.println("c4d: "+demandas.get(idDemanda).campo4.get(c4d)+" ofertas: "+ofertas.get(indice).campo4.get(c4d));
                                    {
                                        try {
                                        simCampo4 += matrizTriangular4[ofertas.get(indice).campo4.get(c4d)
                                                * (n4 - ofertas.get(indice).campo4.get(c4d)) + demandas.get(idDemanda).campo4.get(c4d)];
                                        } catch (Exception ex){
                                            
                                        }
                                        } else {
                                        try {
                                        simCampo4 += matrizTriangular4[demandas.get(idDemanda).campo4.get(c4d) * (n4 - demandas.get(idDemanda).campo4.get(c4d))
                                                + ofertas.get(indice).campo4.get(c4d)];
                                        } catch (Exception ex){
                                            
                                        }
                                    }
                                }

                            }
                        }
                    }
                    // calcular similaridad total resultante con una
                    // proporciÃƒÂ³n especÃƒÂ­fica para cada campo

                    // calcular similaridad total con proporciÃƒÂ³n de cada
                    // campo
                    similaridad_actual =
                            (((campos_activos[0] == 1) ? peso1 * simCampo1 : 0)
                            + ((campos_activos[1] == 1) ? peso2 * simCampo2 : 0)
                            + ((campos_activos[2] == 1) ? peso3 * simCampo3 : 0)
                            + ((campos_activos[3] == 1) ? peso4 * simCampo4 : 0))
                            / (((campos_activos[0] == 1) ? peso1 : 0)
                            + ((campos_activos[1] == 1) ? peso2 : 0)
                            + ((campos_activos[2] == 1) ? peso3 : 0)
                            + ((campos_activos[3] == 1) ? peso4 : 0));

                    // acumular similaridad para cada oferta
                    ofertas.get(indice).valorAcumulado = similaridad_actual;//asigna la similaridad de la demanda con la oferta
//                    System.out.println(ofertas.get(indice).valorAcumulado);
//                    System.out.println("---Oferta"+(indice+1)+": "+similaridad_actual+
//                    " ("+simCampo1+", "+simCampo2+", "+simCampo3+", "+simCampo4+")");
                    indice++;
                }

                // Obtener subconjunto de ofertas a recomendar que sobrepasen
                // umbral
                indice = 0;
                ArrayList<Integer> i_recomendaciones = new ArrayList<Integer>();
                while (indice < n) {
                    if (ofertas.get(indice).valorAcumulado >= UMBRAL) {
                        i_recomendaciones.add(new Integer(indice + 1));
                    }
                    indice++;
                }

                for (int i = 0; i < i_recomendaciones.size(); i++) {
                    // System.out.println("Entro a i_recomendaiones");
                    out.write("--Oferta"
                            + i_recomendaciones.get(i) + ", similaridad: "
                            + ofertas.get(i_recomendaciones.get(i) - 1).valorAcumulado + "\n");
                }
                out.write("-----------------------------------------");

                List deofpe = new ArrayList();
                // Ordenar resultados
                int idDe = 0;
                ArrayList<Integer> recomendaciones = quickSort(ofertas,
                        i_recomendaciones);
                out.write("Tiempo de busqueda para demanda con id "
                        + (idDemanda + 1) + ":" + " "
                        + (System.currentTimeMillis() - inicio) + " ms\n");

                // variables agregadas
                idDe = idDemanda + 1;
                float similitud = 0;
                int oferta = 0;
                deofpe.add(idDe);
                // fin variables agregadas
                for (int i = 0; i < recomendaciones.size(); i++) {
                    //deofpe=new ArrayList();
                    // alamacena una demanda con n ofertas y su peso
                    // System.out.println("entro a recomendaciones");

                    out.write("--Oferta"
                            + recomendaciones.get(i)
                            + ", similaridad: "
                            + ofertas.get(recomendaciones.get(i) - 1).valorAcumulado
                            + "\n");
                    // asignaciones agregadas

                    similitud = Float.parseFloat(ofertas.get(recomendaciones.get(i) - 1).valorAcumulado + "");
                    oferta = recomendaciones.get(i);
//                    System.out.println("_____________________________________");
//                   System.out.println("Demnada: " + idDe + " ");
//                    System.out.println("Oferta: " + oferta);
//                    System.out.println("Similitud: " + similitud);


                    deofpe.add(oferta);
                    deofpe.add(similitud);

                    //fin asignaciones agregadas

                }
                ofertasDemandas.add(deofpe);
//                System.out.println(deofpe);
            }// fin del codigo agregado
            // } fin del while para leer el archivo entrada
        } catch (Exception ex){
            ex.printStackTrace();
        }finally {
            //scanner.close();
            out.close();
        }
        return ofertasDemandas;
    }

    
    public List satisfacerDemandasMap(
            String nombreArchivoSalida, Map<Integer,OfertaDemanda> ofertas,
            Map<Integer,OfertaDemanda> demandas, float matrizTriangular1[],
            float matrizTriangular2[], float matrizTriangular3[],
            float matrizTriangular4[], int n1, int n2, int n3, int n4)
            throws IOException, SQLException {
        //System.out.println("entro a satisfacer demandas");

        List ofertasDemandas = new ArrayList();
        DaoFiltrados datos = new DaoFiltrados();// objeto de la clase SQL para ocupar los metodos
        // que ejecutan las sentencias

        Writer out = new OutputStreamWriter(new FileOutputStream(
                nombreArchivoSalida));
        //Scanner scanner = new Scanner(new FileInputStream(nombreArchivoEntrada));
        // Leer cada lÃƒÂ­nea del archivo de entrada
        try {
            DaoFiltrados df = new DaoFiltrados();
            //System.out.println(" Entro al try");
            float UMBRAL = 0.0f;
            float similaridad_actual = 0.0f;
            float simCampo1 = 0.0f;
            float simCampo2 = 0.0f;
            float simCampo3 = 0.0f;
            float simCampo4 = 0.0f;
            int indice = 0;
            //int n = 60;
            int n = ofertas.size();
            //		int indiceDemanda;// idDemanda
            //	int ncampos;// nfactores
            int campos_activos[] = new int[4];
            //int campo;// factor
            //	float proporcion1 = 0, proporcion2 = 0, proporcion3 = 0, proporcion4 = 0;// peso1,peso2,peso3,peso4
            long inicio;
            /*
             * // leer umbral if (scanner.hasNext()) UMBRAL =
             * Float.parseFloat(scanner.next()); System.out.println("UMBRAL: " +
             * UMBRAL);
             */

            // codigo agregado

            UMBRAL = df.obtenerUmbralClasificacion();
            System.out.println("Umbral de clasificacion: "+UMBRAL);
            
            out.write("Umbral General: "+UMBRAL+"\n\n\n");

            // 1.- almacenar los id de demandas a satisfacer
            List demanda = datos.idDemanda();//CON DEMANDA 
            // System.out.println("lista demanda base"+demanda);

            int idDemanda = 0;
            int nfactores;
            int factor = 0;
            float peso = 0;
            List factoresPeso;
            float peso1 = 0, peso2 = 0, peso3 = 0, peso4 = 0;

            for (int id = 0; id < demanda.size(); id++) {
                try{
                //System.out.println("entro a for demandas");

                // se obtiene id de la demanda en especifico
                idDemanda = Integer.parseInt(demanda.get(id) + "");
//                System.out.println("idDemanda: " + idDemanda);
                idDemanda -= 1;

                // 2.- se obtiene los nfactores a considerar por demnada
                nfactores = datos.nFactores(idDemanda + 1);
//                System.out.println("nFactores: " + nfactores);
                // inicialmente no considerar todos los campos (cero)
                for (int cc = 0; cc < 4; cc++) {
                    campos_activos[cc] = 0;
                }

                // System.out.println("1) Sector econÃƒÂ³mico\n2)Actividad\n3)Ãƒï¿½rea TecnolÃƒÂ³gica\n4)Ãƒï¿½rea CientÃƒÂ­fica");
                // se almacenar los factores de cada demanda en 1;
                // 3.- se obtiene todos los factores a considerar
                factoresPeso = datos.factoresPeso(idDemanda + 1);

                for (int ci = 0; ci < factoresPeso.size(); ci++) {
                    List datosFP = (List) factoresPeso.get(ci);//lista factor junto a su peso

                    for (int pf = 0; pf < datosFP.size(); pf++) {
                        // obtener el factor por factor
                        factor = Integer.parseInt(datosFP.get(0) + "");

                        // 4.- se obtiene el peso por cada factor.

                        campos_activos[factor - 1] = 1;
                        peso = Float.parseFloat(datosFP.get(1).toString());

                        if (factor == 1) {
                            //System.out.println("entro f1");
                            peso1 = Float.parseFloat(datosFP.get(1).toString());

                        }
                        if (factor == 2) {
                            //  System.out.println("entro f2");
                            peso2 = Float.parseFloat(datosFP.get(1).toString());
                        }
                        if (factor == 3) {
                            //  System.out.println("entro f3");
                            peso3 = Float.parseFloat(datosFP.get(1).toString());

                        }
                        if (factor == 4) {
                            //System.out.println("entro f4");
                            peso4 = Float.parseFloat(datosFP.get(1).toString());

                        }

                    }
//                    System.out.println("FACTOR: " + factor);
//                    System.out.println("PESO: " + peso);
                }

                // fin de codigo agregado
                // Comenzar a contar tiempo de bÃƒÂºsqueda para satisfacer la
                // demanda actual
                inicio = System.currentTimeMillis();
                // Busqueda de la oferta mas similar
                indice = 0;
                while (indice < n) {

                    // Para cada subcampo de la demanda obtener similaridad con
                    // subcampos de la oferta actual (si es que existen y si
                    // fueron activados para la bÃƒÂºsqueda)
                    if (campos_activos[0] == 1) {
                        simCampo1 = 0.0f;
                        if (demandas.get(idDemanda).campo1 != null
                                && ofertas.get(indice).campo1 != null) {
                            // System.out.println("Prueba: " + demandas.get(idDemanda).campo1);
                        }
                        //System.out.println("Prueba2: " + ofertas.get(indice).campo1);
                        for (int c1d = 0; c1d < demandas.get(idDemanda).campo1.size(); c1d++) {
                            if (c1d < ofertas.get(indice).campo1.size()) {
                                if (demandas.get(idDemanda).campo1.get(c1d) >= ofertas.get(indice).campo1.get(c1d)) {
                                    simCampo1 += matrizTriangular1[ofertas.get(indice).campo1.get(c1d)
                                            * (n1 - ofertas.get(indice).campo1.get(c1d))
                                            + demandas.get(idDemanda).campo1.get(c1d)];
                                } else {
                                    simCampo1 += matrizTriangular1[demandas.get(idDemanda).campo1.get(c1d)
                                            * (n1 - demandas.get(idDemanda).campo1.get(c1d))
                                            + ofertas.get(indice).campo1.get(c1d)];
                                }
                                //System.out.println("------simcampo1: " + simCampo1);
                            }
                        }
                    }
                    if (campos_activos[1] == 1) {
                        simCampo2 = 0.0f;
                        if (demandas.get(idDemanda).campo2 != null
                                && ofertas.get(indice).campo2 != null) //System.out.println("Prueba: "+demandas.get(idDemanda));
                        //System.out.println("Prueba2: "+ofertas.get(indice));
                        {
                            for (int c2d = 0; c2d < demandas.get(idDemanda).campo2.size(); c2d++) {
                                if (c2d < ofertas.get(indice).campo2.size()) {
                                    if (demandas.get(idDemanda).campo2.get(c2d) >= ofertas.get(indice).campo2.get(c2d)) {
                                        simCampo2 += matrizTriangular2[ofertas.get(indice).campo2.get(c2d)
                                                * (n2 - ofertas.get(indice).campo2.get(c2d))
                                                + demandas.get(idDemanda).campo2.get(c2d)];
                                    } else {
                                        simCampo2 += matrizTriangular2[demandas.get(idDemanda).campo2.get(c2d)
                                                * (n2 - demandas.get(idDemanda).campo2.get(c2d))
                                                + ofertas.get(indice).campo2.get(c2d)];
                                    }
                                    // System.out.println("------simcampo2: "+simCampo2);
                                }

                            }
                        }
                    }
                    if (campos_activos[2] == 1) {
                        simCampo3 = 0.0f;
                        if (demandas.get(idDemanda).campo3 != null
                                && ofertas.get(indice).campo3 != null) {
                            for (int c3d = 0; c3d < demandas.get(idDemanda).campo3.size(); c3d++) {
                                if (c3d < ofertas.get(indice).campo3.size()) {
                                    if (demandas.get(idDemanda).campo3.get(c3d) >= ofertas.get(indice).campo3.get(c3d)) {
                                        simCampo3 += matrizTriangular3[ofertas.get(indice).campo3.get(c3d)
                                                * (n3 - ofertas.get(indice).campo3.get(c3d))
                                                + demandas.get(idDemanda).campo3.get(c3d)];
                                    } else {
                                        simCampo3 += matrizTriangular3[demandas.get(idDemanda).campo3.get(c3d)
                                                * (n3 - demandas.get(idDemanda).campo3.get(c3d))
                                                + ofertas.get(indice).campo3.get(c3d)];
                                    }
                                }

                            }
                        }
                    }
                    if (campos_activos[3] == 1) {
                        simCampo4 = 0.0f;
                        if (demandas.get(idDemanda).campo4 != null
                                && ofertas.get(indice).campo4 != null) {
                            for (int c4d = 0; c4d < demandas.get(idDemanda).campo4.size(); c4d++) {
                                if (c4d < ofertas.get(indice).campo4.size()) {
                                    if (demandas.get(idDemanda).campo4.get(c4d) >= ofertas.get(indice).campo4.get(c4d)) //System.out.println("c4d: "+demandas.get(idDemanda).campo4.get(c4d)+" ofertas: "+ofertas.get(indice).campo4.get(c4d));
                                    {
                                        try {
                                        simCampo4 += matrizTriangular4[ofertas.get(indice).campo4.get(c4d)
                                                * (n4 - ofertas.get(indice).campo4.get(c4d)) + demandas.get(idDemanda).campo4.get(c4d)];
                                        } catch (Exception ex){
                                            
                                        }
                                        } else {
                                        try {
                                        simCampo4 += matrizTriangular4[demandas.get(idDemanda).campo4.get(c4d) * (n4 - demandas.get(idDemanda).campo4.get(c4d))
                                                + ofertas.get(indice).campo4.get(c4d)];
                                        } catch (Exception ex){
                                            
                                        }
                                    }
                                }

                            }
                        }
                    }
                    // calcular similaridad total resultante con una
                    // proporciÃƒÂ³n especÃƒÂ­fica para cada campo

                    // calcular similaridad total con proporciÃƒÂ³n de cada
                    // campo
                    similaridad_actual =
                            (((campos_activos[0] == 1) ? peso1 * simCampo1 : 0)
                            + ((campos_activos[1] == 1) ? peso2 * simCampo2 : 0)
                            + ((campos_activos[2] == 1) ? peso3 * simCampo3 : 0)
                            + ((campos_activos[3] == 1) ? peso4 * simCampo4 : 0))
                            / (((campos_activos[0] == 1) ? peso1 : 0)
                            + ((campos_activos[1] == 1) ? peso2 : 0)
                            + ((campos_activos[2] == 1) ? peso3 : 0)
                            + ((campos_activos[3] == 1) ? peso4 : 0));

                    // acumular similaridad para cada oferta
                    ofertas.get(indice).valorAcumulado = similaridad_actual;
//                    System.out.println(ofertas.get(indice).valorAcumulado);
//                    System.out.println("---Oferta"+(indice+1)+": "+similaridad_actual+
//                    " ("+simCampo1+", "+simCampo2+", "+simCampo3+", "+simCampo4+")");
                    indice++;
                }

                // Obtener subconjunto de ofertas a recomendar que sobrepasen
                // umbral
                indice = 0;
                ArrayList<Integer> i_recomendaciones = new ArrayList<Integer>();
                while (indice < n) {
                    if (ofertas.get(indice).valorAcumulado >= UMBRAL) {
                        i_recomendaciones.add(new Integer(indice + 1));
                    }
                    indice++;
                }

                for (int i = 0; i < i_recomendaciones.size(); i++) {
                    // System.out.println("Entro a i_recomendaiones");
                    out.write("--Oferta"
                            + i_recomendaciones.get(i) + ", similaridad: "
                            + ofertas.get(i_recomendaciones.get(i) - 1).valorAcumulado + "\n");
                }
                out.write("-----------------------------------------");

                List deofpe = new ArrayList();
                // Ordenar resultados
                int idDe = 0;
                ArrayList<Integer> recomendaciones = null;
//                ArrayList<Integer> recomendaciones = quickSort(ofertas,
//                        i_recomendaciones);
                out.write("Tiempo de busqueda para demanda con id "
                        + (idDemanda + 1) + ":" + " "
                        + (System.currentTimeMillis() - inicio) + " ms\n");

                // variables agregadas
                idDe = idDemanda + 1;
                float similitud = 0;
                int oferta = 0;
                deofpe.add(idDe);
                // fin variables agregadas
                for (int i = 0; i < recomendaciones.size(); i++) {
                    //deofpe=new ArrayList();
                    // alamacena una demanda con n ofertas y su peso
                    // System.out.println("entro a recomendaciones");

                    out.write("--Oferta"
                            + recomendaciones.get(i)
                            + ", similaridad: "
                            + ofertas.get(recomendaciones.get(i) - 1).valorAcumulado
                            + "\n");
                    // asignaciones agregadas

                    similitud = Float.parseFloat(ofertas.get(recomendaciones.get(i) - 1).valorAcumulado + "");
                    oferta = recomendaciones.get(i);
//                    System.out.println("_____________________________________");
//                   System.out.println("Demnada: " + idDe + " ");
//                    System.out.println("Oferta: " + oferta);
//                    System.out.println("Similitud: " + similitud);


                    deofpe.add(oferta);
                    deofpe.add(similitud);

                    //fin asignaciones agregadas

                }
                ofertasDemandas.add(deofpe);
//                System.out.println(deofpe);
                } catch(NullPointerException np){
                }
            }// fin del codigo agregado
            // } fin del while para leer el archivo entrada
        } catch (Exception ex){
            ex.printStackTrace();
        }finally {
            //scanner.close();
            out.close();
        }
        return ofertasDemandas;
    }
    
    /**
     * Ordenamiento quick sort (devuelve lista de ÃƒÂ­ndices ordenados de
     * ofertas de recomendaciÃƒÂ³n)
     *
     * @param ofertas ArrayList<OfertaDemanda>
     * @param indices ArrayList<Integer>
     * @return ArrayList<Integer>
     */
    static ArrayList<Integer> quickSort(List<OfertaDemanda> ofertas, ArrayList<Integer> indices) {
        //System.out.println("entro a  quickSort");
        if (indices.size() <= 1) {
            return indices;
        }
        // obtener y remover pivote
        int pivote = indices.get(indices.size() / 2) - 1;
        pivote = pivote < 0 ? 0 : pivote;
        indices.remove(indices.size() / 2);
        // llenar lado menor y lado mayor con respecto al pivote
        ArrayList<Integer> menor = new ArrayList<Integer>();
        ArrayList<Integer> mayor = new ArrayList<Integer>();
        for (int i = 0; i < indices.size(); i++) {
            // usar >= para orden descendente y <= para orden ascendente
            if (ofertas.get(indices.get(i) - 1).valorAcumulado >= ofertas.get(pivote).valorAcumulado) {
                menor.add(new Integer(indices.get(i)));
            } else {
                mayor.add(new Integer(indices.get(i)));
            }
        }
        // concatenar resultado
        ArrayList<Integer> resultado = new ArrayList<Integer>();
        menor = quickSort(ofertas, menor);
        mayor = quickSort(ofertas, mayor);
        menor.add(pivote + 1);
        resultado.addAll(menor);
        resultado.addAll(mayor);

        return resultado;
    }

//    
//    /**
//     * Obtener instancias de ofertas/demandas a partir de una ontologÃƒÂ­a
//     *
//     * @param model OntModel
//     * @param URI String
//     * @param campo1 String
//     * @param campo2 String
//     * @param campo3 String
//     * @param campo4 String
//     * @param capacidad int
//     * @param ofertaDemanda String
//     * @return ArrayList<OfertaDemanda>
//     */
//    public ArrayList<OfertaDemanda> obtenerOfertasDemandas(OntModel model,
//            String URI, String campo1, String campo2, String campo3,
//            String campo4, int capacidad, String ofertaDemanda) {
//        //System.out.println("Entro a obtenerofertascemandas");
//
//
//        ArrayList<OfertaDemanda> o_d = new ArrayList<OfertaDemanda>(capacidad);
//        // inicializar ofertas
//        for (int i = 0; i < capacidad; i++) {
//            o_d.add(new OfertaDemanda(new ArrayList<Integer>(),
//                    new ArrayList<Integer>(), new ArrayList<Integer>(),
//                    new ArrayList<Integer>()));
//        }
//        // consultar instancias de ofertas
//        // -----------------------------------------------------------
//        Query query = QueryFactory.create("PREFIX p1: <" + URI + "#> "
//                + "SELECT  ?ind " + "WHERE { ?ind a p1:" + ofertaDemanda
//                + "SectorEconomico . " + "?ind p1:" + campo1 + " ?att "
//                + " FILTER( ?att!='Null' ) " + "}");
//
//        // Execute the query and obtain results
//        QueryExecution qe = QueryExecutionFactory.create(query, model);
//        com.hp.hpl.jena.query.ResultSet results = qe.execSelect();
//        QuerySolution qs;
//        int indiceOferta = 0;
//        // Output query results
//        while (results.hasNext()) {
//            qs = results.next();
//            // System.out.println(qs.get("ind").toString());
//            indiceOferta = Integer.parseInt(qs.get("ind").toString().substring(qs.get("ind").toString().indexOf("SE") + 2,
//                    qs.get("ind").toString().indexOf("_")));
//            o_d.get(indiceOferta - 1).campo1.add(new Integer(Integer.parseInt(qs.get("ind").toString().substring(
//                    qs.get("ind").toString().indexOf("idd_") + 4))));
//       //System.out.println("sector economico: "+qs);
//        }
//        
//        qe.close();
//        query = QueryFactory.create("PREFIX p1: <" + URI + "#> "
//                + "SELECT  ?ind " + "WHERE { ?ind a p1:" + ofertaDemanda
//                + "Actividad . " + "?ind p1:" + campo2 + " ?att "
//                + " FILTER( ?att!='Null' ) " + "}");
//        // Execute the query and obtain results
//        qe = QueryExecutionFactory.create(query, model);
//        results = qe.execSelect();
//        // Output query results
//        while (results.hasNext()) {
//            qs = results.next();
//            // System.out.println(qs.get("ind").toString());
//            indiceOferta = Integer.parseInt(qs.get("ind").toString().substring(qs.get("ind").toString().indexOf("A") + 1,
//                    qs.get("ind").toString().indexOf("_")));
//            o_d.get(indiceOferta - 1).campo2.add(new Integer(Integer.parseInt(qs.get("ind").toString().substring(
//                    qs.get("ind").toString().indexOf("idd_") + 4))));
////        System.out.println("actividad: "+indiceOferta);
//           // System.out.println("sector economico: "+qs);
//        }
//        qe.close();
//        query = QueryFactory.create("PREFIX p1: <" + URI + "#> "
//                + "SELECT  ?ind " + "WHERE { ?ind a p1:" + ofertaDemanda
//                + "AreaTecnologica . " + "?ind p1:" + campo3 + " ?att "
//                + " FILTER( ?att!='Null' ) " + "}");
//        // Execute the query and obtain results
//        qe = QueryExecutionFactory.create(query, model);
//        results = qe.execSelect();
//        // Output query results
//        while (results.hasNext()) {
//            qs = results.next();
//            // System.out.println(qs.get("ind").toString());
//            indiceOferta = Integer.parseInt(qs.get("ind").toString().substring(qs.get("ind").toString().indexOf("AT") + 2,
//                    qs.get("ind").toString().indexOf("_")));
//            o_d.get(indiceOferta - 1).campo3.add(new Integer(Integer.parseInt(qs.get("ind").toString().substring(
//                    qs.get("ind").toString().indexOf("idd_") + 4))));
////        System.out.println("Area tecnologica: "+indiceOferta
//         //   System.out.println("sector economico: "+qs);
//        }
//        qe.close();
//        query = QueryFactory.create("PREFIX p1: <" + URI + "#> "
//                + "SELECT  ?ind " + "WHERE { ?ind a p1:" + ofertaDemanda
//                + "AreaCientifica . " + "?ind p1:" + campo4 + " ?att "
//                + " FILTER( ?att!='Null' ) " + "}");
//        // Execute the query and obtain results
//        qe = QueryExecutionFactory.create(query, model);
//        results = qe.execSelect();
//        // Output query results
//        while (results.hasNext()) {
//            qs = results.next();
//            // System.out.println(qs.get("ind").toString());
//            indiceOferta = Integer.parseInt(qs.get("ind").toString().substring(qs.get("ind").toString().indexOf("AC") + 2,
//                    qs.get("ind").toString().indexOf("_")));
//            o_d.get(indiceOferta - 1).campo4.add(new Integer(Integer.parseInt(qs.get("ind").toString().substring(
//                    qs.get("ind").toString().indexOf("idd_") + 4))));
////       System.out.println("area cientifica: "+indiceOferta);
//         //   System.out.println("sector economico: "+qs);
//        }
//        qe.close();
//        return o_d;
//    }
//
//    /**
//     * MÃƒÂ©todo para obtener el nombre del nodo raÃƒÂ­z a partir de un modelo y
//     * sus URI's
//     *
//     * @param model OntModel
//     * @param URI_relacion String
//     * @param URI_clase String
//     * @param relacion String
//     * @return String
//     */
//    public String obtenerRaiz(OntModel model, String URI_relacion,
//            String URI_clase, String relacion) {
//
//        //System.out.println("entro a obtenerRaiz");
//
//        Query query = QueryFactory.create("PREFIX p1: <" + URI_relacion + "#> "
//                + "SELECT  ?a " + "WHERE { ?a_hija p1:" + relacion + " ?a ."
//                + " minus {?a p1:" + relacion + " ?a_padre} " + "}");
//        // System.out.println(query);
//        // Execute the query and obtain results
//        QueryExecution qe = QueryExecutionFactory.create(query, model);
//        com.hp.hpl.jena.query.ResultSet results = qe.execSelect();
//        QuerySolution qs;
//        String nodoRaiz = "";
//        // Output query results
//        if (results.hasNext()) {
//            qs = results.next();
//            nodoRaiz = qs.get("a").toString().substring(URI_clase.length() + 1);
//            // System.out.println(nodoRaiz);
//        }
//        qe.close();
//        return nodoRaiz;
//    }

    /**
     * MÃƒÂ©todo para generar una matriz de similaridad
     *
     * @param n int
     * @param tabla TablaRuteo
     * @param mensajeTitulo String
     * @param nombreArchivo String
     * @return float []
     * @throws IOException
     */
    public float[] generarMatrizSimilaridad(int n, TablaRuteo tabla,
            String mensajeTitulo, String nombreArchivo, String path) throws IOException {
        float matrizTriangular[] = null;
        try {
            matrizTriangular = new float[n * (n - 1) / 2];
        // System.out.println("entro a generarmatrizsimilaridad");

        float p = 0.1f;
        Writer out = new OutputStreamWriter(//instancia del OutputStreamWriter
                new FileOutputStream(//Instanciar flujo del archivo
                (nombreArchivo.equals("")) ? path+"UltimaMatriz.txt"//si el nombre del arhivo que recibe no contiene nada se utiliza el nombre UltimateMatriz
                : nombreArchivo));//si no está vacio se se deja el nombre que ya trae del comienzo
        try {
//            System.out.println("Matriz de similaridad (triangular) ***********");
            out.write("---------------------------------------------------------\n");
            out.write(mensajeTitulo + "\n");
            out.write("---------------------------------------------------------\n");
            // para cada par de registros (sin repetir)
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < i; j++) {
                    out.write("-");
                }
                for (int j = i; j < n; j++) {
                    // obtener distancia semÃƒÂ¡ntica para esta celda y
                    // aplicarle
                    // la siguiente funciÃƒÂ³n para transformarla a similaridad
                    float val = 1.0f / (p
                            * distanciaSemantica(tabla.registros.get(i),
                            tabla.registros.get(j)) + 1.0f);
                    
//                    matrizTriangular[i * (n - i) + j] = 1.0f / (p
//                            * distanciaSemantica(tabla.registros.get(i),
//                            tabla.registros.get(j)) + 1.0f);
                    
                    matrizTriangular[i * (n - i) + j] = val;
                    
                    
                    out.write(matrizTriangular[i * (n - i) + j] + "(" + i + ","
                            + j + "),");
                
                
//                    System.out.println("I: "+i+"J: "+j);
//                    System.out.println("PosMatriz: "+i * (n - i) + j);
//                    System.out.println("PosValor: "+val);
//                    System.out.println("Writing: "+matrizTriangular[i * (n - i) + j] + "(" + i + ","
//                            + j + "),");
//                    System.out.println("--------------");
                }
                out.write("\n");
            }
        } finally {
            out.close();
        }
        } catch (Exception ex){
            ex.printStackTrace();
            
        } 
        return matrizTriangular;
    }

    /**
     * MÃƒÂ©todo para obtener la distancia semÃƒÂ¡ntica a partir de dos
     * registros
     *
     * @param Nodo1 Registro
     * @param Nodo2 Registro
     * @return float
     */
    public float distanciaSemantica(Registro Nodo1, Registro Nodo2) {

//        System.out.println("Entro distanciasemantica");
//        System.out.println("Nodo1: "+Nodo1.nodo);
//        System.out.println("Nodo1: "+Nodo2.nodo);
        if (Nodo1 == Nodo2) {
            return 0.0f;
        } else {

            float distancia_minima = MAXIMA_DISTANCIA;

            String[] ruta1;
            String[] pesos1;
            boolean e1_encontrado;
            String[] ruta2;
            String[] pesos2;
            float distancia_n1_a_e1;
            float distancia_n2_a_e2;

            // para cada ruta r1 de nodo1 hacia nodoRaÃƒÂ­z
            for (int r1 = 0; r1 < Nodo1.ruta_hacia_raiz.size(); r1++) {
                ruta1 = Nodo1.ruta_hacia_raiz.get(r1).split(",");
                pesos1 = Nodo1.pesos_en_ruta.get(r1).split(",");
                distancia_n1_a_e1 = 0.0f;
                // para cada elemento e1 en ruta r1
                for (int e1 = 0; e1 < ruta1.length; e1++) {
                    e1_encontrado = false;
                    distancia_n1_a_e1 += Float.parseFloat(pesos1[e1]);
                    // para cada ruta r2 de nodo2 hacia nodoRaÃƒÂ­z
                    for (int r2 = 0; r2 < Nodo2.ruta_hacia_raiz.size(); r2++) {
                        ruta2 = Nodo2.ruta_hacia_raiz.get(r2).split(",");
                        pesos2 = Nodo2.pesos_en_ruta.get(r2).split(",");
                        distancia_n2_a_e2 = 0.0f;
                        // para cada elemento e2 en ruta r2
                        for (int e2 = 0; e2 < ruta2.length; e2++) {
                            distancia_n2_a_e2 += Float.parseFloat(pesos2[e2]);
                            // si elemento e2 es igual a elemento e1
                            if (ruta2[e2].contentEquals(ruta1[e1])) {
                                // si distancia de nodo1 a nodo2 <
                                // distancia_minima (actualizarla)
                                if (distancia_n1_a_e1 + distancia_n2_a_e2 < distancia_minima) {
                                    distancia_minima = distancia_n1_a_e1
                                            + distancia_n2_a_e2;
                                }
                                e1_encontrado = true;
                                // terminar de buscar en la ruta actual r2
                                break;
                            }
                        }

                    }
                    // si se encontro elemento terminar de buscar en ruta actual
                    // r1
                    if (e1_encontrado) {
                        break;
                    }
                }
            }
            // devolver la distancia mÃƒÂ­nima entre nodo1 y nodo2
            return distancia_minima;
        }

    }

      
}
