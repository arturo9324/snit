/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia;

//import com.hp.hpl.jena.ontology.OntModel;
//import com.hp.hpl.jena.ontology.OntModelSpec;
//import com.hp.hpl.jena.rdf.model.ModelFactory;
import mx.gob.saidt.motorInferencia.bean.OfertaDemanda;
import mx.gob.saidt.motorInferencia.dao.DaoFiltrados;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author
 */
public class Filtrados {

    static List matrizRecurso = new ArrayList();
    static List matrizInfraestructura = new ArrayList();
    static List matrizObjetoConocimiento = new ArrayList();

    public void filtrados(String path) throws IOException, SQLException {
        FiltradoClasificacion fc = new FiltradoClasificacion();
        FiltradoRecursosAsociados fr = new FiltradoRecursosAsociados();
        FiltradoTexto ft = new FiltradoTexto();
        FiltradoVigencia fv = new FiltradoVigencia();
        DaoFiltrados sql = new DaoFiltrados();

        long inicio = System.currentTimeMillis();//cuanti tiempo tarda en ejecutar el fistrado
        
        CalculoCatalogo cc = new CalculoCatalogo();
        ArchivoSalida as = new ArchivoSalida();

        // subdominio 1
        System.out.println("Tabla de ruteo 1: ");
        /* //**Inicio bloque omitido
         TablaRuteo tabla1 = new TablaRuteo(nodoRaiz1, model1, URI1_clase,
         URI1_relacion, relacion1);
        
         //tabla1.llenarTabla();
         */ //** Fin bloque omitido

        TablaRuteo tabla1 = new TablaRuteo();
        tabla1.registros = cc.calculosCatalogo("Economic_Activity", 4);//retorna todo el arbol de catalogo en forma de lista
        tabla1.mostrarTablaArchivo(path + "tablaRuteo1-ActividadEconomica.txt");//imprime el arbol de ruteo en el archivo especificado

        //cc.mostrarTabla(tabla1.registros);
        //tabla1.mostrarTabla();
        System.out.println("------------------");
//        try {
//            tabla1.mostrarTablaArchivo(path + "tabla1.txt");
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        // subdominio 2
        System.out.println("Tabla de ruteo 2: ");
        /* //**Inicio bloque omitido
         TablaRuteo tabla2 = new TablaRuteo(nodoRaiz2, model2, URI2_clase,
         URI2_relacion, relacion2);
         //tabla2.llenarTabla();
         */ //** Fin bloque omitido
        TablaRuteo tabla2 = new TablaRuteo();
        tabla2.registros = cc.calculosCatalogo("Activity", 3);
        tabla2.mostrarTablaArchivo(path + "tablaRuteo2-Actividad.txt");

        //cc.mostrarTabla(tabla2.registros);
        //tabla2.mostrarTabla();
        System.out.println("------------------");
//        try {
//            tabla2.mostrarTablaArchivo(path + "tabla2.txt");
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        // subdominio 3
        System.out.println("Tabla de ruteo 3: ");
        /* //**Inicio bloque omitido
         TablaRuteo tabla3 = new TablaRuteo(nodoRaiz3, model3, URI3_clase,
         URI3_relacion, relacion3);
         //tabla3.llenarTabla();
         */ //** Fin bloque omitido
        TablaRuteo tabla3 = new TablaRuteo();
        tabla3.registros = cc.calculosCatalogo("TechnologicalField", 3);
        tabla3.mostrarTablaArchivo(path + "tablaRuteo3-AreaTecnologica.txt");

        //cc.mostrarTabla(tabla3.registros);
        //tabla3.mostrarTabla();
        System.out.println("------------------");
//        try {
//            tabla3.mostrarTablaArchivo(path + "tabla3.txt");
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        // subdominio 4
        System.out.println("Tabla de ruteo 4: ");
        /* //**Inicio bloque omitido
         TablaRuteo tabla4 = new TablaRuteo(nodoRaiz4, model4, URI4_clase,
         URI4_relacion, relacion4);
         //tabla4.llenarTabla();
         */ //** Fin bloque omitido
        TablaRuteo tabla4 = new TablaRuteo();
        tabla4.registros = cc.calculosCatalogo("KnowledgeField", 3);
        tabla4.mostrarTablaArchivo(path + "tablaRuteo4-AreaCientifica.txt");

        //cc.mostrarTabla(tabla4.registros);
        // tabla4.mostrarTabla();
        System.out.println("------------------");
//        try {
//            tabla4.mostrarTablaArchivo(path + "tabla4.txt");
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

        // subdominio 1
        System.out.println("Tabla de ruteo recursos: ");
        /* //**Inicio bloque omitido
         TablaRuteo tablaruteoRecursos = new TablaRuteo(nodoRaiz5, model1,
         URI_clase, URI_relacion, relacion5);

         tablaruteoRecursos.llenarTabla();

         //         tablaruteoRecursos.mostrarTabla();
         tablaruteoRecursos.mostrarTabla();
         */ //** Fin bloque omitido

        TablaRuteo tablaruteoRecursos = new TablaRuteo();
        tablaruteoRecursos.registros = cc.calculosCatalogo("Resource", 1);
        tablaruteoRecursos.mostrarTablaArchivo(path + "tablaRuteo5-Recursos.txt");

        //cc.mostrarTabla(tablaruteoRecursos.registros);
        System.out.println("------------------");
//        try {
//            tablaruteoRecursos.mostrarTablaArchivo(path + "ruteoRecursos.txt");
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

        // subdominio 2
        System.out.println("Tabla de ruteo funcional: ");
        /* //**Inicio bloque omitido
         TablaRuteo tablaRuteoFuntional = new TablaRuteo(nodoRaiz6, model1,
         URI_clase, URI_relacion, relacion6);
         tablaRuteoFuntional.llenarTabla();
        
         tablaRuteoFuntional.mostrarTabla();
         */ //** Fin bloque omitido

        TablaRuteo tablaRuteoFuntional = new TablaRuteo();
        tablaRuteoFuntional.registros = cc.calculosCatalogo("FuntionalPlace", 1);
        tablaRuteoFuntional.mostrarTablaArchivo(path + "tablaRuteo6-Infraestructura.txt");

        //cc.mostrarTabla(tablaRuteoFuntional.registros);
        System.out.println("------------------");

//        try {
//            tablaRuteoFuntional.mostrarTablaArchivo(path + "ruteoFuntionalPlace.txt");
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        // subdominio 3	
//        TablaRuteo tablaRuteoObjetoConocimiento = new TablaRuteo(nodoRaiz7, model_1,
//                URI_conocimiento, URI_relacion2, relacion7);
//        tablaRuteoObjetoConocimiento.llenarTabla();
//        // tabla2.mostrarTabla();
//        try {
//            tablaRuteoObjetoConocimiento.mostrarTablaArchivo("ruteoObjetoConocimiento.txt");
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        // 4)Se crean las matrices de similaridad
        // ----------------------------------------------------------
        // ----Generar matriz triangular de distancia semÃƒÂ¡ntica------
        // ----y convertirla a matriz triangular de similaridad------
        // ----------------------------------------------------------
        // matriz 1}
        System.out.println("Matriz 1");
        int n1 = tabla1.registros.size();
        float matrizTriangular1[] = fc.generarMatrizSimilaridad(n1, tabla1,
                "Matriz 1", path + "matriz1.txt", path);
        // matriz 2
        System.out.println("Matriz 2");
        int n2 = tabla2.registros.size();
        float matrizTriangular2[] = fc.generarMatrizSimilaridad(n2, tabla2,
                "Matriz 2", path + "matriz2.txt", path);
        // matriz 3
        System.out.println("Matriz 3");
        int n3 = tabla3.registros.size();
        float matrizTriangular3[] = fc.generarMatrizSimilaridad(n3, tabla3,
                "Matriz 3", path + "matriz3.txt", path);
        // matriz 4
        System.out.println("Matriz 4");
        int n4 = tabla4.registros.size();
        float matrizTriangular4[] = fc.generarMatrizSimilaridad(n4, tabla4,
                "Matriz 4", path + "matriz4.txt", path);

        int n5 = tablaruteoRecursos.registros.size();

        System.out.println("Matriz 5 (Recursos)");
        System.out.println("N5: " + n5);
        float matrizTriangular5[] = generarMatrizSimilaridad(n5,
                tablaruteoRecursos, "Matriz 1", path + "matrizRecursos.txt", path);

        // matriz 2
        System.out.println("Matriz 6");
        int n6 = tablaRuteoFuntional.registros.size();
        float matrizTriangular6[] = generarMatrizSimilaridad(n6,
                tablaRuteoFuntional, "Matriz 2", path + "matrizFuntionalPlace.txt", path);

        //Causa de error
        // matriz 3
//        int n7 = tablaRuteoObjetoConocimiento.registros.size();
//        float matrizTriangular7[] = generarMatrizSimilaridad(n7,
//                tablaRuteoObjetoConocimiento, "Matriz 3", "matrizObjetoConocimiento.txt");
        // Dada una demanda y un conjunto de ofertas, buscar la oferta que
        // mÃƒÂ¡s se parezca a la demanda
        // buscada (15,7,8,14) <- valores mÃƒÂ¡ximos
        System.out.println("Tiempo de ejecuciÃƒÂ³n de la primera fase: "
                + (System.currentTimeMillis() - inicio) + " milisegundos");
        System.out.println("Matrices 1,2,3 y 4 con n1: " + n1 + ", n2: " + n2
                + ", n3: " + n3 + ", n4: " + n4);

        /*
         // 5)Se obtienen las ofertas y demandas
         // Obtener Ofertas
         ArrayList<OfertaDemanda> ofertas = fc.obtenerOfertasDemandas(model_oferta,
         URI_oferta, "idSectorEconomico", "idActividad",
         "idAreaTecnologica", "idAreaCientifica", 83, "Oferta");
         System.out.println("Ofertas extraidas: " + ofertas.size());

         for (int i = 0; i < ofertas.size(); i++) {
         OfertaDemanda od = ofertas.get(i);
         System.out.println("Campo 1 (ActividadEconomica): " + od.campo1);
         System.out.println("Campo 2 (Actividad): " + od.campo2);
         System.out.println("Campo 3: " + od.campo3);
         System.out.println("Campo 4: " + od.campo4);
         System.out.println("Valor: " + od.valorAcumulado);
         System.out.println("----------------------");
         }

         ArrayList<OfertaDemanda> demandas = fc.obtenerOfertasDemandas(
         model_oferta, URI_oferta, "idSectorEconomico", "idActividad",
         "idAreaTecnologica", "idAreaCientifica", 79, "Demanda");

         System.out.println("Demandas extraidas: " + demandas.size());

         for (int i = 0; i < demandas.size(); i++) {
         OfertaDemanda od = demandas.get(i);
         System.out.println("Campo 1: " + od.campo1);
         System.out.println("Campo 2: " + od.campo2);
         System.out.println("Campo 3: " + od.campo3);
         System.out.println("Campo 4: " + od.campo4);
         System.out.println("Valor: " + od.valorAcumulado);
         System.out.println("-------------------------------");
         }
         */
        //Se obtienen las ofertas y demandas desde la base de datos
        ArrayList<OfertaDemanda> demandas = cc.realizaCalculosDemandaId();//obtiene las demanada activas
        System.out.println("Demandas: " + demandas.size());

        ArrayList<OfertaDemanda> ofertas = cc.realizaCalculosOfertaId();//obtiene ofertas activas
        System.out.println("Ofertas: " + ofertas.size());

        // 6)Se satisfacen las demandas mediante comparación con ofertas
        //FILTRADO POR CLASIFICACION
        System.out.println("Filtro de clasificacion");
        List ofertasDemandas = fc.satisfacerDemandas(path + "1.FiltroClasificacion.txt", ofertas, demandas,
                matrizTriangular1, matrizTriangular2, matrizTriangular3,
                matrizTriangular4, n1, n2, n3, n4);

        System.out.println("Demandas clasificacion: " + ofertasDemandas.size());

//        for (int i = 0; i < ofertasDemandas.size(); i++) {
//            System.out.println("Filtrado clasificación: " + ofertasDemandas.get(i));
//        }
//      
        ofertasDemandas = fr.ordenarId(ofertasDemandas);//regresa lista ordenada por el id de la demanda

        System.out.println("Demandas odenarId: " + ofertasDemandas.size());

        //FILTADO POR VIGENCIA
        System.out.println("Filtrado Vigencia");
        List ofdeFilVigencia = fv.ofertasVigentes(ofertasDemandas);//regresa las demndas con sus odertas vigentes
        as.generaFiltroVigencia(path + "2.FiltroVigencia.txt", ofdeFilVigencia, fv.ofertas);

        //FILTRADO POR TEXTO
        //ft.DemandaOferaTexto(recursosAsociados, path);
        System.out.println("Filtrado texto");
        ft.DemandaOferaTexto(ofdeFilVigencia, path);
        ft.ejecutarFreeling();
        List listaPalabras = ft.tfIdf(path);
        listaPalabras = ft.coseno(listaPalabras);

//        System.out.println("-- Resultado coseno --");
//        for (int i = 0; i < listaPalabras.size(); i++) {
//            List demandaOfertas = (List) listaPalabras.get(i);
//            List demand = (List) demandaOfertas.get(0);
//            List ofers = (List) demandaOfertas.get(1);
//            System.out.println("Demanda: " + demand.get(0));
//            if (ofers.size() > 0) {
//                for (int j = 0; j < ofers.size(); j++) {
//                    List ofer = (List) ofers.get(j);
//                    if (ofer.size() > 0) {
//                        List idOfer = (List) ofer.get(0);
//                        System.out.println("IdOfer: " + idOfer);
//                        double cos = (Double) ofer.get(ofer.size() - 1);
//                        System.out.println("Cos: " + ofer.get(ofer.size() - 1));
////                        System.out.println("Oferta (Id: " + idOfer + ", Cos: " + cos + "): " + ofer);
//                    }
//                }
//            }
//            System.out.println("-------------------------------------------");
//        }
        as.generaFiltroTextoCoseno(path + "3.1.FiltroTexto-Coseno.txt", listaPalabras);

        System.out.println("Eliminacion por umbral");
        listaPalabras = ft.eliminarUmbral(listaPalabras);

//        System.out.println("-- Resultado de eliminación --");
//
//        for (int i = 0; i < listaPalabras.size(); i++) {
//            List demandaOfertas = (List) listaPalabras.get(i);
//            List demand = (List) demandaOfertas.get(0);
//            List ofers = (List) demandaOfertas.get(1);
//            System.out.println("Demanda: " + demand.get(0));
//            if (ofers.size() > 0) {
//                for (int j = 0; j < ofers.size(); j++) {
//                    List ofer = (List) ofers.get(j);
//                    System.out.println("Oferta: " + ofer);
//                }
//            } else {
//                System.out.println("Sin Ofertas");
//            }
//        }
        as.generaFiltroTextoEliminacion(path + "3.2.FiltroTexto-EliminacionUmbral.txt", listaPalabras);

        System.out.println("Fotmato a lista");
        List filtroTexto = ft.formatoLista(listaPalabras);

        System.out.println("Suma de coseno");
        filtroTexto = ft.sumarCosenoOferta(ofdeFilVigencia, listaPalabras);

        //FILTRADO POR RECURSOS ASOCIADOS
        System.out.println("Filtrado recursos");
        List demandasOfertasRecuros = fr.demandasRecursosAsociados(matrizRecurso, "recursoTecnologico", filtroTexto);//retorna los recursos asociados con todo y sus pesos
//        for (int i = 0; i < demandasOfertasRecuros.size(); i++) {
//            System.out.println("DemandasOfertasRecursos: " + demandasOfertasRecuros.get(i));
//        }

        as.generaFiltroRecursos(path + "4.1.FiltroRecursosAsociados-Tecnologico.txt", demandasOfertasRecuros, "Recurso Tecnologico");

        List demandasOfertasInfraestructura = fr.demandasRecursosAsociados(matrizInfraestructura, "infraestructura", filtroTexto);
//        for (int i = 0; i < demandasOfertasInfraestructura.size(); i++) {
//            System.out.println("demandasOfertasInfraestructura: " + demandasOfertasInfraestructura.get(i));
//        }

        as.generaFiltroRecursos(path + "4.2.FiltroRecursosAsociados-Infraestructura.txt", demandasOfertasInfraestructura, "Infraestructura");

        List recursosAsociados = fr.recursosAsociados(demandasOfertasRecuros, demandasOfertasInfraestructura);//retorna una lista con los pesos de la infrestructura y los recursos tecnológicos sumados
//        for (int i = 0; i < recursosAsociados.size(); i++) {
//            System.out.println("Filtrado Rcursos asociados: " + recursosAsociados.get(i));
//        }

//        for (int i = 0; i < recursosAsociados.size(); i++) {
//            System.out.println("Filtrado Recursos: " + recursosAsociados.get(i));
//        }
        as.generaFiltroRecursos(path + "4.3.FiltroRecursosAsociados-Tecnologico+Infraestructura.txt", recursosAsociados, "Tecnologico+Infraestructura");

        // 7.- INSERTAR DEMANDAS Y OFERTAS SALIENTES DE LOS FILTRADOS
        System.out.println("Insersion");
        sql.insertOfertaDemanda(recursosAsociados, path + "5.InsersionResultados.txt");// se insertan demnadas a la base de datos
        
        System.out.println("Finalizado");
    }

    static float[] generarMatrizSimilaridad(int n, TablaRuteo tabla,
            String mensajeTitulo, String nombreArchivo, String path) throws IOException {

        FiltradoClasificacion fc = new FiltradoClasificacion();
        float matrizTriangular[] = new float[n * (n - 1) / 2];
        float p = 0.1f;
        Writer out = new OutputStreamWriter(
                new FileOutputStream((nombreArchivo.equals("")) ? path + "UltimaMatriz.txt" : nombreArchivo));
        try {
            out.write("---------------------------------------------------------\n");
            out.write(mensajeTitulo + "\n");
            out.write("---------------------------------------------------------\n");
            // para cada par de registros (sin repetir)
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < i; j++) {
                    out.write("-");
                }
                for (int j = i; j < n; j++) {
                    // obtener distancia semÃƒÂ¡ntica para esta celda y
                    // aplicarle
                    // la siguiente funciÃƒÂ³n para transformarla a similaridad

                    matrizTriangular[i * (n - i) + j] = 1.0f / (p * fc.distanciaSemantica(tabla.registros.get(i), tabla.registros.get(j)) + 1.0f);

                    out.write(matrizTriangular[i * (n - i) + j] + "(" + i + "," + j + "),");

                    String cadena = 1.0f / (p * fc.distanciaSemantica(tabla.registros.get(i), tabla.registros.get(j)) + 1.0f) + "(" + i + "," + j + ")";

                    if (mensajeTitulo.equals("Matriz 1")) {
                        matrizRecurso.add(cadena);
                    } else if (mensajeTitulo.equals("Matriz 2")) {
                        matrizInfraestructura.add(cadena);
                    } else {
                        matrizObjetoConocimiento.add(cadena);
                    }

                }

                out.write("\n");
            }
        } finally {
            out.close();
        }
        return matrizTriangular;
    }
}
