/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia;

import mx.gob.saidt.motorInferencia.dao.CalculaCatalogoDao;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author erick-mirsha
 */
public class ArchivoSalida {

    public void generaFiltroVigencia(String archivo, List lista, List noVigentes) {
        try {
            Writer out = new OutputStreamWriter(new FileOutputStream(archivo));

            out.write("==Ofertas no vigentes==\n");
            for (int i = 0; i < noVigentes.size(); i++) {
                out.write(noVigentes.get(i) + ", ");
            }

            out.write("\n\n==Resultado==\n");
            for (int i = 0; i < lista.size(); i++) {
                List deof = (List) lista.get(i);
                //System.out.println("Demanda: "+deof.get(0)+"\n");
                out.write("Demanda: " + deof.get(0) + "\n");
                out.write("Oferta\tPesoClasificacion\n");
                for (int j = 1; j < deof.size(); j++) {
                    List ofer = (List) deof.get(j);
                    //System.out.println("Oferta: "+ofer);
                    out.write(ofer.get(0) + "\t" + ofer.get(1) + "\n");
                }
            }
            out.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void generaFiltroTextoCoseno(String archivo, List listaPalabrasCoseno) {
        try {
            Writer out = new OutputStreamWriter(new FileOutputStream(archivo));

            out.write("==Resultado coseno==\n");
            for (int i = 0; i < listaPalabrasCoseno.size(); i++) {
                List demandaOfertas = (List) listaPalabrasCoseno.get(i);
                List demand = (List) demandaOfertas.get(0);
                List ofers = (List) demandaOfertas.get(1);
                List demWrite = (List) demand.get(0);
                out.write("Demanda: " + demWrite.get(0) + "\n");
                out.write("Oferta\tCoseno\n");
                if (ofers.size() > 0) {
                    for (int j = 0; j < ofers.size(); j++) {
                        List ofer = (List) ofers.get(j);
                        if (ofer.size() > 0) {
                            List idOfer = (List) ofer.get(0);
                            double cos = (Double) ofer.get(ofer.size() - 1);
                            out.write(idOfer.get(0) + "\t" + ofer.get(ofer.size() - 1) + "\n");
//                        System.out.println("Oferta (Id: " + idOfer + ", Cos: " + cos + "): " + ofer);
                        }
                    }
                }
                out.write("\n\n");
            }
            out.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void generaFiltroTextoEliminacion(String archivo, List listaPalabrasCoseno) {
        try {
            CalculaCatalogoDao ccd = new CalculaCatalogoDao();
            Writer out = new OutputStreamWriter(new FileOutputStream(archivo));

            out.write("==Resultado de eliminacion por umbral==\n");

            for (int i = 0; i < listaPalabrasCoseno.size(); i++) {
                List demandaOfertas = (List) listaPalabrasCoseno.get(i);
                List demand = (List) demandaOfertas.get(0);
                List ofers = (List) demandaOfertas.get(1);
                List demWrite = (List) demand.get(0);
                float umbral = ccd.extraeUmbralTd(Integer.parseInt(demWrite.get(0).toString()));
                out.write("Demanda: " + demWrite.get(0) + "\tUmbral: " + umbral + "\n");
                out.write("Oferta\tCoseno\n");
                if (ofers.size() > 0) {
                    for (int j = 0; j < ofers.size(); j++) {
                        List ofer = (List) ofers.get(j);
                        if (ofer.size() > 0) {
                            List idOfer = (List) ofer.get(0);
                            double cos = (Double) ofer.get(ofer.size() - 1);
                            out.write(idOfer.get(0) + "\t" + ofer.get(ofer.size() - 1) + "\n");
                        }
                    }
                } else {
                    out.write("Sin Ofertas\n");
                }
            }

            out.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void generaFiltroRecursos(String archivo, List demandasOfertas,String tipo) {
        try {
            Writer out = new OutputStreamWriter(new FileOutputStream(archivo));//instancias para escibir archivo en la ruta y con el nombre 

            out.write("==Resultado de filtro de recurso ("+tipo+") ==\n");//escribir titulo

            for (int i = 0; i < demandasOfertas.size(); i++) {

                List deof = (List) demandasOfertas.get(i);
                int de = Integer.parseInt(deof.get(0) + "");//determina la demanda
                out.write("Demanda: " + de + "\n");
                out.write("Oferta\tPeso\n");

                try {
                    if (deof.size() > 1) {

                        for (int j = 1; j < deof.size(); j++) {
                            List ofertaPeso = (List) deof.get(j);
                            out.write(ofertaPeso.get(0)+"\t"+ofertaPeso.get(1)+"\n");
                        }
                        
                    } else {
                        out.write("Sin Ofertas\n");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            out.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
