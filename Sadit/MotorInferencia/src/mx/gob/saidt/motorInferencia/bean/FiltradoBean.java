package mx.gob.saidt.motorInferencia.bean;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 
 */
public class FiltradoBean {
    private String demanda;
    private String oferta;
    private String tipoEstatus;
    private String fecha;

    public String getDemanda() {
        return demanda;
    }

    public void setDemanda(String demanda) {
        this.demanda = demanda;
    }

    public String getOferta() {
        return oferta;
    }

    public void setOferta(String oferta) {
        this.oferta = oferta;
    }

    public String getTipoEstatus() {
        return tipoEstatus;
    }

    public void setTipoEstatus(String tipoEstatus) {
        this.tipoEstatus = tipoEstatus;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
    
}
