/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.saidt.motorInferencia.bean;

import java.util.ArrayList;

/**
 * Clase OfertaDemanda que representa una oferta o demanda tecnolÃ³gica
 * con 4 campos (simulando un registro de la base de datos). Cada campo
 * puede tener 0 o varios valores, los cuales son enteros que hacen referencia
 * a el id en la base de datos.
 * AdemÃ¡s se guarda un campo del valor acumulado para acumular el grado de similitud
 * entre una oferta y una demanda a la hora de satisfacer una demanda.
 * @author
 *
 */
public class OfertaDemanda{
	public ArrayList<Integer>campo1;
	public ArrayList<Integer>campo2;
	public ArrayList<Integer>campo3;
	public ArrayList<Integer>campo4;
	public float valorAcumulado;



	public OfertaDemanda(ArrayList<Integer> c1, ArrayList<Integer> c2, ArrayList<Integer> c3, ArrayList<Integer> c4){
		campo1 = c1;
		campo2 = c2;
		campo3 = c3;
		campo4 = c4;
		valorAcumulado = 0.0f;
	}
}
