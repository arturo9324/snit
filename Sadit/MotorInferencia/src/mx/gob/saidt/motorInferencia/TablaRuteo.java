/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia;

//import com.hp.hpl.jena.ontology.OntModel;
//import com.hp.hpl.jena.query.Query;
//import com.hp.hpl.jena.query.QueryExecution;
//import com.hp.hpl.jena.query.QueryExecutionFactory;
//import com.hp.hpl.jena.query.QueryFactory;
//import com.hp.hpl.jena.query.QuerySolution;
import mx.gob.saidt.motorInferencia.bean.Registro;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Clase TablaRuteo para almacenar las rutas que hay entre cada nodo
 * bajo el subdominio tratado y su nodo raÃ­z, asÃ­ como la distancia semÃ¡ntica
 * entre cada nodo consecutivo en esas rutas.
 * @author 
 */
public class TablaRuteo {

    ArrayList<Registro> registros;
    ArrayList<String> rutas = new ArrayList<String>();
    ArrayList<String> pesoS = new ArrayList<String>();
    String nodoRaiz;
//    OntModel model;
    String URI_clase;
    String URI_relacion;
    String relacion;

//    TablaRuteo(String nR, OntModel m, String URI_c, String URI_r, String r) {
//        nodoRaiz = nR;
//        model = m;
//        URI_clase = URI_c;
//        URI_relacion = URI_r;
//        relacion = r;
//        registros = new ArrayList<Registro>();
//
////         System.out.println("Nodo raiz " + nodoRaiz);
////         System.out.println("Model  " + model);
////        System.out.println("uri clase " +  URI_clase);
////        System.out.println("uri relación  " + URI_relacion);
////        System.out.println(" relación  " + relacion);
//    }


//    /**
//     * MÃ©todo para llenar esta tabla de ruteo
//     */
//    void llenarTabla() {
//        rutas = new ArrayList<String>();
//        pesoS = new ArrayList<String>();
//        rutas.add(nodoRaiz);
//        pesoS.add("0.0");
//        //aÃ±adir nodo raÃ­z a la tabla
//        registros.add(new Registro(nodoRaiz, rutas, pesoS));
//        //aÃ±adir el resto de los nodos
//
//        LlenarTablaPorAnchura(nodoRaiz, 0, nodoRaiz, "2.0",
//                "PREFIX p1: <" + URI_relacion + "#> "
//                + "PREFIX p2: <" + URI_clase + "#> SELECT  ?a "
//                + "WHERE { ?a p1:" + relacion + " p2:", "a");
//    }

    /**
     * MÃ©todo para mostrar el contenido de esta tabla de ruteo en salida estÃ¡ndar
     */
    void mostrarTabla() {
        /*
        System.out.println("|\tNodo\t\t|\tRuta\t\t\t|\tPesos\t\t|");
        for (int i = 0; i < registros.size(); i++) {
            System.out.print(registros.get(i).nodo + "\t");
            if (registros.get(i).ruta_hacia_raiz != null) {
                for (int j = 0; j < registros.get(i).ruta_hacia_raiz.size(); j++) {
                    System.out.print("{" + registros.get(i).ruta_hacia_raiz.get(j) + "} ");
                }
            }
            if (registros.get(i).pesos_en_ruta != null) {
                for (int j = 0; j < registros.get(i).pesos_en_ruta.size(); j++) {
                    System.out.print("(" + registros.get(i).pesos_en_ruta.get(j) + ") ");
                }
            }
            System.out.println();
        }
        */
        
        //Mostrar variables de tabla
        for (int i = 0; i < registros.size(); i++) {
            Registro r = registros.get(i);
            
            System.out.println("Nodo: "+r.nodo);
            System.out.println("RutaRaiz: "+r.ruta_hacia_raiz);
            System.out.println("PesosRuta: "+r.pesos_en_ruta);
           
            System.out.println("-------------------------");
        }
    }

    /**
     * MÃ©todo para mostrar el contenido de esta tabla de ruteo en un archivo
     * @param nombreArchivo
     * @throws IOException
     */
    void mostrarTablaArchivo(String nombreArchivo) throws IOException {

        Writer out = new OutputStreamWriter(new FileOutputStream(nombreArchivo));
        try {
            out.write("|\tNodo\t\t|\tRuta\t\t\t|\tPesos\t\t|\n");
            for (int i = 0; i < registros.size(); i++) {
                out.write(registros.get(i).nodo + "\t");
                if (registros.get(i).ruta_hacia_raiz != null) {
                    for (int j = 0; j < registros.get(i).ruta_hacia_raiz.size(); j++) {
                        out.write("{" + registros.get(i).ruta_hacia_raiz.get(j) + "} ");
                    }
                }
                if (registros.get(i).pesos_en_ruta != null) {
                    for (int j = 0; j < registros.get(i).pesos_en_ruta.size(); j++) {
                        out.write("(" + registros.get(i).pesos_en_ruta.get(j) + ") ");
                    }
                }
                out.write("\n");
            }
        } finally {
            out.close();
        }
    }

    /**
     * MÃ©todo para obtener el Ã­ndice de un nodo a partir de su nombre
     * @param nombre String
     * @return int
     */
    int obtenerIndice(String nombre) {
        for (int i = 0; i < registros.size(); i++) {
            if (registros.get(i).nodo.contentEquals(nombre)) {
                return i;
            }
        }
        return -1;
    }

//    
//    /**
//     * MÃ©todo para llenar recursivamente en recorrido por Anchura la tabla de ruteo
//     * @param nombrePadre String
//     * @param profundidad int
//     * @param ruta String
//     * @param pesos String
//     * @param consulta String
//     * @param campo String
//     */
//    //-----------------------------------------------------------------------
//    void LlenarTablaPorAnchura(String nombrePadre, int profundidad,
//            String ruta, String pesos, String consulta, String campo) {
//        // Consultar hijos del padre recibido
//        //System.out.println(consulta+nombrePadre+" }");
//        Query query = QueryFactory.create(consulta + nombrePadre + " }");
//
//
//        QueryExecution qe = QueryExecutionFactory.create(query, model);
//        com.hp.hpl.jena.query.ResultSet results = qe.execSelect();
//        QuerySolution qs;
//        String nombreCampo = "";
//        boolean rutaAlternativa;
//        ArrayList<String> hijos = new ArrayList<String>();
//        // Iterar sobre los registros de la consulta
//        while (results.hasNext()) {
//            qs = results.next();
//            //System.out.println("QS TablaRuteo: "+qs);
//            nombreCampo = qs.get(campo).toString().substring(URI_clase.length() + 1);
//            rutaAlternativa = false;
//            //Para cada registro ri en tablaRuteo
//            for (int i = 0; i < registros.size(); i++) {
//                // Si ri tiene el mismo nombre que el
//                // registro consultado en la iteraciÃ³n actual
//                if (registros.get(i).nodo.contentEquals(nombreCampo)) {
//                    registros.get(i).ruta_hacia_raiz.add(nombreCampo + "," + ruta);
//                    registros.get(i).pesos_en_ruta.add("0.0," + pesos);
//                    rutaAlternativa = true;
//                    break;
//                }
//            }
//            //si no se encontrÃ³ en la tabla
//            //agregar el registro de la iteraciÃ³n
//            //actual con primer ruta y pesos a tablaRuteo
//            if (!rutaAlternativa) {
//                ArrayList<String> rutas = new ArrayList<String>();
//                ArrayList<String> pesoS = new ArrayList<String>();
//                rutas.add(nombreCampo + "," + ruta);
//                pesoS.add("0.0," + pesos);
//                registros.add(new Registro(nombreCampo, rutas, pesoS));
//            }
//            //agregar el nodo hijo del registro actual
//            //a lista de hijos
//            hijos.add(nombreCampo);
//        }
//        qe.close();
//        //Para cada hijo aplicar el mismo proceso (se convierten en padres en la siguiente llamada)
//        for (int i = 0; i < hijos.size(); i++) {
//            LlenarTablaPorAnchura(hijos.get(i), profundidad + 1, hijos.get(i) + "," + ruta,
//                    (1.0 + 1.0 / (Math.pow(2.0, (double) (profundidad + 1)))) + "," + pesos, consulta, campo);
//        }
//    }
}
