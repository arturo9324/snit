/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia;

import mx.gob.saidt.motorInferencia.dao.DaoFiltrados;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.gob.saidt.util.ObtenerRutaDirectorios;

/**
 *
 * @author
 */
public class EjecutarFiltrados {
    public void ejecuta() {
        try {
            DaoFiltrados df = new DaoFiltrados();//Instanciar clase DaoFiltrados
            Filtrados f = new Filtrados();//Instanciar clase Filtrados
            ObtenerRutaDirectorios rutas = new ObtenerRutaDirectorios();//Instanciar clase ObtenerRutaDirectorios
                    
            String path = "";//Variable path(ruta) inicializada en nada
            
            System.out.println("Obtener Ruta del directorio 'MotorInferencia'");//
            path = rutas.obtenerRutaOntologias();//ejecutar método obtenerRutaOntologías 
            /*en este punto lo que se realiza es obtener el archivo saidt.properties 
            donde se obtienen los atributos del sistema que se estára ejecutando, 
            en este caso se obtendra la ruta*/
            System.out.println(path);

            Script sh = new Script();//instancia la clase script
            boolean okS = sh.generarScript();//se ejecuta el método que genera el script
            System.out.println("Crear script? " + okS);
            if (okS) {
                List demandasUtilizadas = df.demandasFiltrado();//se obtinen todas las demandas que esten modificadas o aceptadas
                if (demandasUtilizadas.size() > 0) {// se verifica que la lista tenga algo
                    try {
                        f.filtrados(path);
                    } catch (IOException ex) {
                        Logger.getLogger(EjecutarFiltrados.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SQLException ex) {
                        Logger.getLogger(EjecutarFiltrados.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    System.out.println("No hay ofertas nuevas o procesadas sin resultados");
                }
            } else {
                System.out.println("No se pudo crear el script");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String args[]) {
        EjecutarFiltrados ef = new EjecutarFiltrados();//crear instancia de la clase EjecutarFiltrados
        ef.ejecuta();//Iniciar la función ejecutar
    }
}
