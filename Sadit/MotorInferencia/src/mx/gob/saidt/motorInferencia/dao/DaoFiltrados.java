/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia.dao;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.gob.saidt.motorInferencia.bean.FiltradoBean;
import mx.gob.saidt.motorInferencia.bean.FiltradoBean;
import mx.gob.saidt.util.ConnectionPool;

/**
 *
 * @author
 */
public class DaoFiltrados {


    public List idDemanda() throws SQLException {

        Connection con = ConnectionPool.getConexion();// conexion a la base
        // ejecutar la consulta para obtener todos lo id de las demandas a
        // satisfacer
        String selectSQL = "SELECT FactoresPreferencia.fkDemanda FROM FactoresPreferencia "
                + "INNER JOIN Demanda on FactoresPreferencia.fkDemanda=idDemanda "
                + "INNER JOIN DemandaSolicitud on idDemanda=DemandaSolicitud.fkDemanda "
                + "WHERE (fkTipoStatusSolicitud=3 OR fkTipoStatusSolicitud=9) AND Demanda.status=1 AND FactoresPreferencia.estado = 1 "
                + "GROUP BY fkDemanda;";
        /*String selectSQL = "SELECT FactoresPreferencia.fkDemanda FROM FactoresPreferencia "
                + "INNER JOIN NececidadTecnologica on FactoresPreferencia.fkDemanda=idNecesidadTecnologica "
                + "INNER JOIN DemandaSolicitud on idNecesidadTecnologica=DemandaSolicitud.fkDemanda "
                + "WHERE (fkTipoStatusSolicitud=3 OR fkTipoStatusSolicitud=9) AND Demanda.habilitado=1 AND FactoresPreferencia.estado = 1 "
                + "GROUP BY fkDemanda;";*/
        //selecciona las llaves foraneas de las demandas en la tabla factores preferencia
        //y lo une a la tabla demanda cuando las id's sean iguales
        //y lo une a la tabla demandaSolicitud cuando las id's sean iguales
        //cuando el tipo de solicitud sea 3 o 9 y el status de la demanda sea 1 y el estado del factorPreferencia sea 1 
        //y los agrupa por la id de la demanda

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(selectSQL);
        // lista para almacenar los id de la consulta
        List demanda = new ArrayList();
        while (rs.next()) {
            demanda.add(rs.getInt(1));
        }
        rs.close();
        stmt.close();
        con.close();
        // re retorna la lista con los id
        return demanda;
    }
    
    public int maximoId() throws SQLException {
        int max = 0;
        Connection con = ConnectionPool.getConexion();// conexion a la base
        // ejecutar la consulta para obtener todos lo id de las demandas a
        // satisfacer
        String selectSQL = "SELECT MAX(idDemanda) as maximo FROM Demanda ";

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(selectSQL);
        // lista para almacenar los id de la consulta
        List demanda = new ArrayList();
        while (rs.next()) {
            max = rs.getInt(1);
        }
        rs.close();
        stmt.close();
        con.close();
        // re retorna la lista con los id
        return max;
    }

    public List factoresPeso(int idDemanda) throws SQLException {
        Connection con = ConnectionPool.getConexion();// conexion a la base
        // consulta para obtener los factores de una demanda en especifico
        String selectSQL = "SELECT fkfactor, peso FROM FactoresPreferencia WHERE  fkDemanda="
                + idDemanda + " AND fkfactor < 5 AND estado = 1;";
        /*String selectSQL = "SELECT fkfactor, peso FROM FactoresPreferencia WHERE  fkDemanda="
                + idDemanda + " AND fkfactor < 5 AND estado = 1;";*/

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(selectSQL);
        // lista para almacenar los factores
        List factoresPeso = new ArrayList();

        while (rs.next()) {
            List datosFP = new ArrayList();
            // se almacenan los factores y pesos obtenidos en una lista
            datosFP.add(rs.getInt(1));
            datosFP.add(rs.getFloat(2));
            // se almacena la lista en otra lista para retornar una solo lista
            // de datos
            factoresPeso.add(datosFP);
        }

        rs.close();
        stmt.close();
        con.close();
        // se retorna la lista con los factores obtenirdos
        return factoresPeso;

    }

    public List factoresPesoRecursosTecnologicos(int idDemanda)
            throws SQLException {
        Connection con = ConnectionPool.getConexion();// conexion a la base
        // consulta para obtener los factores de una demanda en especifico
        String selectSQL = "SELECT fkfactor, peso FROM FactoresPreferencia WHERE  fkDemanda=" + idDemanda + " AND fkfactor > 4 AND estado = 1;";

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(selectSQL);
        // lista para almacenar los factores
        List factoresPeso = new ArrayList();

        while (rs.next()) {
            List datosFP = new ArrayList();
            // se almacenan los factores y pesos obtenidos en una lista
            datosFP.add(rs.getInt(1));
            datosFP.add(rs.getFloat(2));
            // se almacena la lista en otra lista para retornar una solo lista
            // de datos
            factoresPeso.add(datosFP);
        }

        rs.close();
        stmt.close();
        con.close();
        // se retorna la lista con los factores obtenirdos
        return factoresPeso;

    }

    public List obtenerUmbralPeso(int idFactor) {
        List factoresPeso = new ArrayList();

        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT idFactorUmbralPeso,etiqueta,umbralPeso FROM FactorUmbralPeso WHERE fkFactor =" + idFactor;

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                List datosFP = new ArrayList();
                // se almacenan los factores y pesos obtenidos en una lista
                datosFP.add(rs.getInt(1));
                datosFP.add(rs.getString(2));
                datosFP.add(rs.getFloat(3));
                // se almacena la lista en otra lista para retornar una solo lista
                // de datos
                factoresPeso.add(datosFP);
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // se retorna la lista con los factores obtenirdos
        return factoresPeso;

    }

    public int nFactores(int idDemanda) throws SQLException {
        Connection con = ConnectionPool.getConexion();// conexion a la base
        // consulta para obtener los nfactores de una demanda
        String selectSQL = "SELECT COUNT( fkDemanda ) FROM FactoresPreferencia WHERE fkDemanda=" + idDemanda + " AND estado = 1;";
        //String selectSQL = "SELECT COUNT( fkDemanda ) FROM FactoresPreferencia WHERE fkDemanda=" + idDemanda + " AND estado = 1;";

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(selectSQL);
        // variable para almacenar los nfactores
        int nfactores = 0;

        while (rs.next()) {
            // se almacenan los nfactores obtenidos en una variable
            nfactores = rs.getInt(1);
        }
        rs.close();
        stmt.close();
        con.close();
        // se retorna la lista con los factores obtenirdos
        return nfactores;
    }

    public void insertOfertaDemanda(List ofertaDemanda, String archivo) throws SQLException {
        Connection con = null;
        DaoFiltrados sql = new DaoFiltrados();
        int idDemanda = 0;
        int idOfertaDemanda = 0;
        String fecha = "";
        Calendar cal1 = Calendar.getInstance();
        fecha = cal1.get(1) + "-" + ((Integer.parseInt(cal1.get(2) + "")) + 1) + "-" + cal1.get(5);

        try {
            Writer out = new OutputStreamWriter(new FileOutputStream(archivo));

            out.write("== Registro de resultados en la base de datos ==\n");

            con = ConnectionPool.getConexion();// conexion a la base
            PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO OfertaDemanda (fkDemanda,fkOferta,similitud,status) VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            //PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO CapacidadNecesidad (fkCapacidadTecnologica,fkNecesidadTecnologica,similitud,fecha,registro) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            PreparedStatement pstmtIns = con.prepareStatement("INSERT INTO DemandaSolicitud (fkDemanda,fkTipoStatusSolicitud,fecha,status) VALUES (?,?,?,?)");
            //PreparedStatement pstmtIns = con.prepareStatement("INSERT INTO SolicitudNecesidad (fkNecesidadTecnologica,fkStatusNecesidadCapacidad,observaciones,fecha,registro) VALUES (?,?,?,?,?)");
            
            PreparedStatement pst = con.prepareStatement("UPDATE DemandaSolicitud SET  status =? WHERE fkDemanda =?;");
            //PreparedStatement pst = con.prepareStatement("UPDATE SolicitudNecesidad SET  registro =? WHERE fkNecesidadTecnologica =?;");
            PreparedStatement pstm = con.prepareStatement("UPDATE  OfertaDemanda SET  status =? WHERE fkDemanda =?;");
            //PreparedStatement pstm = con.prepareStatement("UPDATE  CapacidadNecesidad SET  registro =? WHERE fkNecesidadTecnologica =?;");

            for (int i = 0; i < ofertaDemanda.size(); i++) {
                //Obtener valor de similitud

                //if()
                int cont = 0;
                List ofde = (List) ofertaDemanda.get(i);//almacena por separado cada demanda con su oferta
                idDemanda = Integer.parseInt(ofde.get(0) + "");//almacena el id de la demanda

                out.write("\n\nDemanda: " + idDemanda + "\n");

                out.write("\tBaja logica de registros en 'OfertaDemanda'\n");
                pstm.setInt(1, 0);
                pstm.setInt(2, idDemanda);
                boolean ok1 = pstm.executeUpdate() == 1;

//                System.out.println("Actualizando OfertaDemanda: idDemanda=" + idDemanda + " estatus=0? " + ok1);
                //out.write("");
                out.write("\t\t" + pstm.toString() + "\n");
                out.write("\t\tActualizacion " + (ok1 ? "correcta" : "incorrecta") + "\n");

//                System.out.println("Ofertas: " + (ofde.size() - 1));
                out.write("\tNumero de ofertas: " + (ofde.size() - 1) + "\n");

                if (ofde.size() > 1) {
                    for (int j = 1; j < ofde.size(); j++) {//se reccorren todas sus ofertas
                        List ofsi = (List) ofde.get(j);//se almacena una Oferta con su Similitud
                        out.write("\tOferta: " + ofsi.get(0) + "\tSimilitud: " + ofsi.get(1)+"\n");
                        out.write("\tInsersion de registro en 'OfertaDemanda'\n");
                        cont += 1;
                        try {
                            preparedStatement.setInt(1, idDemanda);//se inserta demanda
                            preparedStatement.setInt(2, Integer.parseInt(ofsi.get(0) + ""));//se inserta oferta
                            preparedStatement.setFloat(3, Float.parseFloat(ofsi.get(1) + ""));//se inserta similitud
                            preparedStatement.setInt(4, 1);//se inserta estado
                            boolean ok3 = preparedStatement.executeUpdate() == 1;

//                            ResultSet idOfertaDemandaGenerado=preparedStatement.getGeneratedKeys();
//                            idOfertaDemandaGenerado.next();
//                            idOfertaDemanda=idOfertaDemandaGenerado.getInt(1);
//                            idOfertaDemandaGenerado.close();
//                            sql.fechaOfertaDemanda(idOfertaDemanda);
//                            System.out.println("se inserto demanda " + idDemanda + " con su oferta: " + ofsi.get(0) + "? " + ok3);
                            out.write("\t\t" + preparedStatement.toString() + "\n");
                            out.write("\t\tInsersion " + (ok3 ? "correcta" : "incorrecta") + "\n");

                        } catch (SQLException e) {
                            out.write("\t\tInsersion incorrecta\n");
//                            System.err.println(" No se inserto demanda " + idDemanda + " con su oferta: " + ofsi.get(0) + "");
                            cont -= 1;
                        }
                    }
                    out.write("\tBaja logica de registros en 'DemandaSolicitud'\n");
                    pst.setInt(1, 0);
                    pst.setInt(2, idDemanda);
                    boolean ok2 = pst.executeUpdate() == 1;

//                    System.out.println("Actualizando DemandaSolicitud: idDemanda=" + idDemanda + " estatus=0? " + ok2);
                    out.write("\t\t" + pst.toString() + "\n");
                    out.write("\t\tActualizacion " + (ok2 ? "correcta" : "incorrecta") + "\n");

                    out.write("\tInsersion de registro en 'DemandaSolicitud'\n");
                    //Insertar en demandaSOlicitud
                    pstmtIns.setInt(1, idDemanda);//se inserta demanda
                    pstmtIns.setInt(2, 10);//tipo estatus solicitud
                    pstmtIns.setString(3, fecha);//Fecha del sistema
                    pstmtIns.setInt(4, 1);//se inserta estado
                    boolean ok = pstmtIns.executeUpdate() == 1;

                    out.write("\t\t" + pstmtIns.toString() + "\n");
                    out.write("\t\tInsersion " + (ok ? "correcta" : "incorrecta") + "\n");

//                    System.out.println("Insertando en DemandaSolicitud: idDemanda=" + idDemanda + " estatus=" + 10 + " fecha=" + fecha + "? " + ok);
//
//                    System.out.println("se inserto demanda " + idDemanda + " con numero de ofertas: " + cont + " de " + (ofde.size() - 1));
                } else {
                    out.write("\tBaja logica de registros en 'DemandaSolicitud'\n");
                    pst.setInt(1, 0);
                    pst.setInt(2, idDemanda);
                    boolean ok2 = pst.executeUpdate() == 1;

                    out.write("\t\t" + pst.toString() + "\n");
                    out.write("\t\tActualizacion " + (ok2 ? "correcta" : "incorrecta") + "\n");

//                    System.out.println("Actualizando DemandaSolicitud: idDemanda=" + idDemanda + " estatus=0? " + ok2);

                    out.write("\tInsersion de registro en 'DemandaSolicitud'\n");
                    //Insertar en demandaSOlicitud
                    pstmtIns.setInt(1, idDemanda);//se inserta demanda
                    pstmtIns.setInt(2, 9);//tipo estatus solicitud
                    pstmtIns.setString(3, fecha);//Fecha del sistema
                    pstmtIns.setInt(4, 1);//se inserta estado
                    boolean ok = pstmtIns.executeUpdate() == 1;

                    out.write("\t\t" + pstmtIns.toString() + "\n");
                    out.write("\t\tInsersion " + (ok ? "correcta" : "incorrecta") + "\n");

//                    System.out.println("Insertando en DemandaSolicitud: idDemanda=" + idDemanda + " estatus=" + 9 + " fecha=" + fecha + "? " + ok);
                }
            }
            pstmtIns.close();
            preparedStatement.close();
            pst.close();
            pstm.close();
            con.close();

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            con.close();
        }
    }

    public List demandasFiltrado() {
        List demandasUtilizadas = new ArrayList();
        Connection con;
        try {
            con = ConnectionPool.getConexion();// conexion a la base
            
            String selectSQL = "SELECT fkDemanda FROM DemandaSolicitud WHERE  fkTipoStatusSolicitud=3 OR fkTipoStatusSolicitud=9";
            /*---------------------------------
            Nueva query= "SELECT fkNecesidadTecnologica FROM SolicitudNecesidad WHERE fkStatusNecesidadCapacidad=3 OR fkStatusNecesidadCapacidad=9;";
            ---------------------------------*/
            //selecciona las llaves foraneas de la tabla DemandaSolicitud donde la llave foranea del status sea igual a 3 o 9
            Statement stmt = con.createStatement();
            //se crea la sentencia para ejecutar la query
            ResultSet rs = stmt.executeQuery(selectSQL);
            //se obtine el resultado de la query
            while (rs.next()) {
                //se agragan todas las demandas a una lista
                demandasUtilizadas.add(rs.getInt(1));

            }

            rs.close();
            stmt.close();
            con.close();

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return demandasUtilizadas;

    }

    public List recursoDemanda(int idDemanda) {
        List demandaRecurso = new ArrayList();
        Connection con;
        try {
            con = ConnectionPool.getConexion();// conexion a la base

            String selectSQL = "SELECT fkTipoRecursoTecnologico FROM DemandaRecurso WHERE  fkDemanda=" + idDemanda;

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {

                demandaRecurso.add(rs.getInt(1));

            }

            rs.close();
            stmt.close();
            con.close();
            // se retorna la lista con los factores obtenirdos
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return demandaRecurso;

    }

    public List recursoOferta(int idOferta) {
        List OfertaRecurso = new ArrayList();
        Connection con;
        try {
            con = ConnectionPool.getConexion();// conexion a la base

            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT DISTINCT fkTipoRecursoTecnologico FROM TipoRecursoTecnologico "
                    + "INNER JOIN RecursoTecnologico ON idTipoRecursoTecnologico = fkTipoRecursoTecnologico "
                    + "INNER JOIN OfertaRecursoTecnologico ON idRecursoTecnologico = fkRecursoTecnologico "
                    + "WHERE  fkOferta=" + idOferta;
            /*String selectSQL = "SELECT DISTINCT fkTipoInstalacion FROM TipoInstalacion "
                    + "INNER JOIN instalacion ON idTipoInstalacion = fkTipoInstalacion "
                -->    + "INNER JOIN OfertaRecursoTecnologico ON idRecursoTecnologico = fkRecursoTecnologico "
                    + "WHERE  fkOferta=" + idOferta;*/

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {

                // se almacenan los factores y pesos  obtenidos en una lista
                OfertaRecurso.add(rs.getInt(1));

            }

            rs.close();
            stmt.close();
            con.close();
            // se retorna la lista con los factores obtenirdos
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return OfertaRecurso;

    }

    public List InfraestructuraOferta(int idOferta) {
        List OfertaRecurso = new ArrayList();
        Connection con;
        try {
            con = ConnectionPool.getConexion();// conexion a la base

            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT DISTINCT InstalacionEspecializada.fkTipoInstalacion FROM TipoInstalacion"
                    + " INNER JOIN InstalacionEspecializada  ON idTipoInstalacion = fkTipoInstalacion"
                    + " INNER JOIN OfertaInstalacion ON idInstalacionEspecializada = fkInstalacionEspecializada"
                    + " WHERE  fkOferta=" + idOferta;
            /*String selectSQL = "SELECT DISTINCT InstalacionEspecializada.fkTipoInstalacion FROM Tipoinfrastructura"
                    + " INNER JOIN InstalacionEspecializada  ON idTipoInfraestructura = fkTipoInstalacion"
                    + " INNER JOIN OfertaInstalacion ON idInstalacionEspecializada = fkInstalacionEspecializada"
                    + " WHERE  fkOferta=" + idOferta;*/
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {

                // se almacenan los factores y pesos  obtenidos en una lista
                OfertaRecurso.add(rs.getInt(1));

            }

            rs.close();
            stmt.close();
            con.close();
            // se retorna la lista con los factores obtenirdos
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return OfertaRecurso;

    }

    public List infraestructuraDemanda(int idDemanda) {
        List demandaRecurso = new ArrayList();
        Connection con;
        try {
            con = ConnectionPool.getConexion();// conexion a la base

            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT fkTipoInstalacion FROM DemandaRecurso WHERE  fkDemanda=" + idDemanda;

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {

                // se almacenan los factores y pesos obtenidos en una lista
                demandaRecurso.add(rs.getInt(1));

            }

            rs.close();
            stmt.close();
            con.close();
            // se retorna la lista con los factores obtenirdos
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return demandaRecurso;

    }

    public List listaDemandas(int idDemanda) {
        List demanda = new ArrayList();
        Connection con;

        String consulta = "select IdDemanda,keywords,titulo,resumen,descripcion "
                + "from Demanda "
                + "where idDemanda=?;";
        /*String consulta = "select IdNecesidadTecnologica,keywords,titulo,descripcion,aplicaciones "
                + "from NecesidadTecnologica "
                + "where idNecesidadTecnologica=?;";*/

        try {
            con = ConnectionPool.getConexion();
            PreparedStatement ps = con.prepareStatement(consulta);
            ps.setInt(1, idDemanda);
            ResultSet rs = (ResultSet) ps.executeQuery();

            while (rs.next()) {

                demanda.add(rs.getInt("idDemanda"));
                demanda.add(rs.getString("titulo"));
                demanda.add(rs.getString("resumen"));
                demanda.add(rs.getString("descripcion"));
                demanda.add(rs.getString("keywords"));
            }

            rs.close();
            ps.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("ERROR [DemandaDao - listaDemandas()] : ");
            e.printStackTrace();
        } finally {
//            try {
//                //con.close();
//            } catch (SQLException ignorar) {
//            }
        }
        return demanda;
    }

    public List listaOfertas(int idOferta) {
        List oferta = new ArrayList();
        Connection con;

        String consulta = "select IdOferta,keywords,titulo,resumen,descripcion from Oferta where idOferta=?;";
        /*String consulta = "select IdCapacidadTecnologica,keywords,titulo,descripcion,aplicaciones from CapacidadTecnologica where idCapacidadTecnologica=?;";*/


        try {
            con = ConnectionPool.getConexion();
            PreparedStatement ps = con.prepareStatement(consulta);

            ps.setInt(1, idOferta);
            ResultSet rs = (ResultSet) ps.executeQuery();

            while (rs.next()) {
                oferta.add(rs.getInt("IdOferta"));
                oferta.add(rs.getString("titulo"));
                oferta.add(rs.getString("resumen"));
                oferta.add(rs.getString("descripcion"));
                oferta.add(rs.getString("keywords"));

            }
            rs.close();
            ps.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("ERROR [OfertaDao - listaOferta()] : ");
            e.printStackTrace();
        } finally {
//            try {
//                if (connect != null) {
//                    connect.close();
//                }
//            } catch (SQLException ignorar) {
//            }
        }
        return oferta;
    }

    public List ofertaRecursoTec(int idOferta) { //se obtiene la descripción y nombre del recurso tecnologico e infraestructura que contiene una oferta
        List ofertasRecurso = new ArrayList();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base            
            String selectSQL = "SELECT nombre,descripcion "
                    + "FROM OfertaRecursoTecnologico "
                    + "INNER JOIN RecursoTecnologico "
                    + "ON idRecursoTecnologico=fkRecursoTecnologico "
                    + "WHERE fkOferta=" + idOferta + ";";

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);

            while (rs.next()) {

                ofertasRecurso.add(rs.getString(1));
                ofertasRecurso.add(rs.getString(2));

            }

            rs.close();
            stmt.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(DaoFiltrados.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ofertasRecurso;
    }

    public List ofertaInfra(int idOferta) {
        List ofertasRecurso = new ArrayList();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            String selectSQL = "SELECT nombre,descripcion "
                    + "FROM InstalacionEspecializada "
                    + "INNER JOIN OfertaInstalacion "
                    + "ON idInstalacionEspecializada=fkInstalacionEspecializada "
                    + "WHERE fkOferta=" + idOferta + ";";

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);

            while (rs.next()) {

                ofertasRecurso.add(rs.getString(1));
                ofertasRecurso.add(rs.getString(2));

            }

            rs.close();
            stmt.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(DaoFiltrados.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ofertasRecurso;
    }

    public List ofertasNOVigentes() {
        List ofertas = new ArrayList();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
// ejecutar la consulta para obtener todos lo id de las demandas 
            // por recurso asociado - recurso tecnologico
            String fecha = "";
            Calendar cal1 = Calendar.getInstance();
            fecha = cal1.get(1) + "-" + ((Integer.parseInt(cal1.get(2) + "")) + 1) + "-" + cal1.get(5);

            String selectSQL = "SELECT idOferta FROM Oferta where fecha_fin < '" + fecha + "';";
            //String selectSQL = "SELECT idCapacidadTecnologica FROM CapacidadTecnologica where fin < '" + fecha + "';";

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los id de la consulta
            while (rs.next()) {
                // se obtienen los valores obtenirdos de la consulta y se almacenan
                // en una lista
                ofertas.add(rs.getInt(1));
            }
            rs.close();
            stmt.close();
            con.close();
            // re retorna la lista con los id

        } catch (SQLException ex) {
            Logger.getLogger(DaoFiltrados.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ofertas;
    }

    public List obtenerOfertaDemanda() {
        List listaDemandasOferta = new ArrayList();
        Connection connect = null;

        String consulta = "SELECT D.titulo AS Demanda,O.titulo AS Oferta, "
                + "(SELECT SS.nombre FROM DemandaSolicitud DS JOIN TipoStatusSolicitud SS ON IdTipoStatusSolicitud = DS.fkTipoStatusSolicitud "
                + "WHERE DS.fkDemanda = OD.fkDemanda AND DS.status = 1 LIMIT 1) AS EstatusSolicitud, "
                + "OD.Fecha "
                + "FROM OfertaDemanda OD "
                + "JOIN Oferta O ON O.idOferta = OD.fkOferta AND OD.status = 1 "
                + "JOIN Demanda D ON D.idDemanda = OD.fkDemanda AND OD.status = 1 "
                + "";

        PreparedStatement ps = null;

        try {
            connect = ConnectionPool.getConexion();
            ps = connect.prepareStatement(consulta);

            ResultSet rs = (ResultSet) ps.executeQuery();

            while (rs.next()) {
                FiltradoBean db = new FiltradoBean();
                db.setDemanda(rs.getString("Demanda"));
                db.setOferta(rs.getString("Oferta"));
                db.setFecha(rs.getString("Fecha"));
                db.setTipoEstatus(rs.getString("EstatusSolicitud"));
                listaDemandasOferta.add(db);
            }

            rs.close();
            ps.close();
            connect.close();

        } catch (SQLException e) {
            System.out.println("ERROR [OfertaDao - getListaOfertas()] : ");
            e.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ignorar) {
            }
        }
        return listaDemandasOferta;
    }

    public boolean modificarHora(int hora) {
        boolean hecho = false;
        Connection connect = null;
        String Su = "update ConfiguracionMi set hora=? where  idConfiguracionMi = 1";

        try {
            connect = ConnectionPool.getConexion();
            PreparedStatement pstmt = connect.prepareStatement(Su);

            pstmt.setInt(1, hora);

            hecho = pstmt.executeUpdate() == 1;
            pstmt.close();

            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hecho;
    }

    public boolean modificarUmbralPeso(int idFp, String etiqueta, float umbralPeso) {
        boolean hecho = false;
        Connection connect = null;
        String Su = "UPDATE FactorUmbralPeso SET etiqueta=?, umbralPeso=? WHERE  idFactorUmbralPeso = ?";

        try {
            connect = ConnectionPool.getConexion();
            PreparedStatement pstmt = connect.prepareStatement(Su);

            pstmt.setString(1, etiqueta);
            pstmt.setFloat(2, umbralPeso);
            pstmt.setInt(3, idFp);

            hecho = pstmt.executeUpdate() == 1;
            pstmt.close();

            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hecho;
    }

    public boolean modificarUmbralGeneral(float umbralPeso) {
        boolean hecho = false;
        Connection connect = null;
        String Su = "UPDATE ConfiguracionMI SET umbralGlobal=? WHERE  idConfiguracionMI = ?";

        try {
            connect = ConnectionPool.getConexion();
            PreparedStatement pstmt = connect.prepareStatement(Su);

            pstmt.setFloat(1, umbralPeso);
            pstmt.setInt(2, 1);

            hecho = pstmt.executeUpdate() == 1;
            pstmt.close();

            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hecho;
    }

    public boolean modificarFechaEjecucion(String fecha) {
        boolean hecho = false;
        Connection connect = null;
        String Su = "update ConfiguracionMi set ultimaEjecucion=? where idConfiguracionMi = 1";

        try {
            connect = ConnectionPool.getConexion();
            PreparedStatement pstmt = connect.prepareStatement(Su);

            pstmt.setString(1, fecha);

            hecho = pstmt.executeUpdate() == 1;
            pstmt.close();

            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hecho;
    }

    public int obtenerHora() {
        int hora = 0;
        Connection connect = null;

        String consulta = "SELECT hora "
                + "FROM ConfiguracionMi "
                + "WHERE idConfiguracionMi = 1";

        PreparedStatement ps = null;

        try {
            connect = ConnectionPool.getConexion();
            ps = connect.prepareStatement(consulta);

            ResultSet rs = (ResultSet) ps.executeQuery();

            while (rs.next()) {
                hora = rs.getInt("hora");
            }

            rs.close();
            ps.close();
            connect.close();

        } catch (SQLException e) {
            System.out.println("ERROR [FiltradosDao - obtenerHora()] : ");
            e.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ignorar) {
            }
        }
        return hora;
    }

    public String obtenerUltimaEjecucion() {
        String fechaHora = "";
        Connection connect = null;

        String consulta = "SELECT ultimaEjecucion "
                + "FROM ConfiguracionMi "
                + "WHERE  idConfiguracionMi = 1";

        PreparedStatement ps = null;

        try {
            connect = ConnectionPool.getConexion();
            ps = connect.prepareStatement(consulta);

            ResultSet rs = (ResultSet) ps.executeQuery();

            while (rs.next()) {
                fechaHora = rs.getString("ultimaEjecucion");
            }

            rs.close();
            ps.close();
            connect.close();

        } catch (SQLException e) {
            System.out.println("ERROR [FiltradosDao - obtenerHora()] : ");
            e.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ignorar) {
            }
        }
        return fechaHora;
    }

    public int obtenerEstatusMotor() {
        int estatus = 0;
        Connection connect = null;

        String consulta = "SELECT  estatusMotor "
                + "FROM ConfiguracionMi "
                + "WHERE idConfiguracionMi = 1"
                + "";

        PreparedStatement ps = null;

        try {
            connect = ConnectionPool.getConexion();
            ps = connect.prepareStatement(consulta);

            ResultSet rs = (ResultSet) ps.executeQuery();

            while (rs.next()) {
                estatus = rs.getInt("estatusMotor");
            }

            rs.close();
            ps.close();
            connect.close();

        } catch (SQLException e) {
            System.out.println("ERROR [OfertaDao - estatusMotor()] : ");
            e.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ignorar) {
            }
        }
        return estatus;
    }

    public boolean modificarEstatusMotor(int estatus) {
        boolean hecho = false;
        Connection connect = null;
        String Su = "update ConfiguracionMi set estatusMotor=? where idConfiguracionMi = 1";

        try {
            connect = ConnectionPool.getConexion();
            PreparedStatement pstmt = connect.prepareStatement(Su);

            pstmt.setInt(1, estatus);

            hecho = pstmt.executeUpdate() == 1;
            pstmt.close();

            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hecho;
    }

    public boolean modificarIdTimer(int id) {
        boolean hecho = false;
        Connection connect = null;
        String Su = "update ConfiguracionMi set idTimer=? where idConfiguracionMi = 1";

        try {
            connect = ConnectionPool.getConexion();
            PreparedStatement pstmt = connect.prepareStatement(Su);

            pstmt.setInt(1, id);

            hecho = pstmt.executeUpdate() == 1;
            pstmt.close();

            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hecho;
    }

    public int obtenerIdTimer() {
        int id = 0;
        Connection connect = null;

        String consulta = "SELECT  idTimer "
                + "FROM ConfiguracionMi "
                + "WHERE idConfiguracionMi = 1"
                + "";

        PreparedStatement ps = null;

        try {
            connect = ConnectionPool.getConexion();
            ps = connect.prepareStatement(consulta);

            ResultSet rs = (ResultSet) ps.executeQuery();

            while (rs.next()) {
                id = rs.getInt("idTimer");
            }

            rs.close();
            ps.close();
            connect.close();

        } catch (SQLException e) {
            System.out.println("ERROR [OfertaDao - estatusMotor()] : ");
            e.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ignorar) {
            }
        }
        return id;
    }

    public String obtenerRutaArchivos() {
        String ruta = "";
        Connection connect = null;

        String consulta = "SELECT rutaArchivos "
                + "FROM ConfiguracionMi "
                + "WHERE  idConfiguracionMi = 1";

        PreparedStatement ps = null;

        try {
            connect = ConnectionPool.getConexion();
            ps = connect.prepareStatement(consulta);

            ResultSet rs = (ResultSet) ps.executeQuery();

            while (rs.next()) {
                ruta = rs.getString("rutaArchivos");

                System.out.println("extraido: " + rs.getString("rutaArchivos"));
            }

            rs.close();
            ps.close();
            connect.close();

        } catch (SQLException e) {
            System.out.println("ERROR [FiltradosDao - obtenerRuta archivos()] : ");
            e.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (SQLException ignorar) {
            }
        }
        return ruta;
    }

    public float obtenerUmbralClasificacion() {
        float umbral = 0.0f;
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base            
            String selectSQL = "SELECT umbralGlobal FROM configuracionMI WHERE idConfiguracionMI = 1";

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);

            while (rs.next()) {
                umbral = rs.getFloat("umbralGlobal");
            }

            rs.close();
            stmt.close();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(DaoFiltrados.class.getName()).log(Level.SEVERE, null, ex);
        }
        return umbral;
    }

}
