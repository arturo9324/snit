/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import mx.gob.saidt.motorInferencia.bean.Registro;
import mx.gob.saidt.util.ConnectionPool;
import mx.gob.saidt.motorInferencia.*;

/**
 *
 * @author erick-mirsha
 */
public class CalculaCatalogoDao {

    int suman1 = 0;
    int suman2 = 0;
    int suman3 = 0;
    int suman4 = 0;

    Connection conn = null;

    public List<Integer> extraerDemandas() {
        List<Integer> lista = new ArrayList<Integer>();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT * FROM Demanda WHERE status = 1 ORDER BY idDemanda";//extraer todas las demandas activas
            /*
                String selectSQL = "SELECT * FROM NecesidadTecnologica WHERE habilitado = 1 ORDER BY idNecesidadTecnologica";
            */
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                lista.add(rs.getInt("idDemanda"));
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return lista;
    }

    public List<Integer> extraerOfertas() {
        List<Integer> lista = new ArrayList<Integer>();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT * FROM Oferta WHERE status = 1 ORDER BY idOferta";//extraer todas las ofertas
            //String selectSQL = "SELECT * FROM CapacidadTecnologica WHERE habilitado = 1 ORDER BY idCapacidadTecnologica";

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                lista.add(rs.getInt("idOferta"));
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return lista;
    }

    public ArrayList<Integer> sumaActividadDemanda(int idDemanda) {
        ArrayList<Integer> suma = new ArrayList<Integer>();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            /*SELECT IF(
	1 IS NULL,
		si cumple, algo
	si no cumpple(
            IF	2 IS NULL,
			si cumple(SELECT COUNT(*) FROM s) + 2,
                        si no cumple(SELECT COUNT(*) FROM s) + (SELECT COUNT(*) FROM s2) +3))
AS Suma FROM s JOIN b on s.i = b.i WHERE b.seleccionado = '" + idOferta + "';";*/
            String selectSQL = "SELECT IF(a.fkActividadN2 IS NULL,a.fkActividadN1,IF(a.fkActividadN3 IS NULL,(SELECT COUNT(*) FROM actividadn1) + a.fkActividadN2,(SELECT COUNT(*) FROM actividadn1) + (SELECT COUNT(*) FROM actividadn2) + a.fkActividadN3)) AS Suma FROM demandaactividad da JOIN actividad a on da.fkActividad = a.idActividad WHERE da.fkDemanda = '" + idDemanda + "';";
            //String selectSQL = "SELECT IF(a.fkActividadN2 IS NULL,a.fkActividadN1,IF(a.fkActividadN3 IS NULL,(SELECT COUNT(*) FROM actividadn1) + a.fkActividadN2,(SELECT COUNT(*) FROM actividadn1) + (SELECT COUNT(*) FROM actividadn2) + a.fkActividadN3)) AS Suma FROM NecesidadTecnologica da JOIN actividad a on da.fkActividad = a.idActividad WHERE da.idNecesidadTecnologica = '" + idDemanda + "';";
            
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                suma.add(rs.getInt("Suma"));
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public ArrayList<Integer> sumaAreaCientificaDemanda(int idDemanda) {
        ArrayList<Integer> suma = new ArrayList<Integer>();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT IF(a.fkAreaCientificaN2 IS NULL,a.fkAreaCientificaN1,IF(a.fkAreaCientificaN3 IS NULL,(SELECT COUNT(*) FROM areacientifican1) + a.fkAreaCientificaN2,(SELECT COUNT(*) FROM areacientifican1) + (SELECT COUNT(*) FROM areacientifican2) + a.fkAreaCientificaN3)) AS Suma FROM demandaareacientifica da JOIN areacientifica a on da.fkAreaCientifica = a.idAreaCientifica  WHERE da.fkDemanda = '" + idDemanda + "';";
            //String selectSQL = "SELECT IF(a.fkAreaCientificaN2 IS NULL,a.fkAreaCientificaN1,IF(a.fkAreaCientificaN3 IS NULL,(SELECT COUNT(*) FROM areacientifican1) + a.fkAreaCientificaN2,(SELECT COUNT(*) FROM areacientifican1) + (SELECT COUNT(*) FROM areacientifican2) + a.fkAreaCientificaN3)) AS Suma FROM NececidadTecnologica da JOIN areacientifica a on da.fkAreaCientifica = a.idAreaCientifica  WHERE da.idNecesidadTecnologica = '" + idDemanda + "';";
            
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                suma.add(rs.getInt("Suma"));
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public ArrayList<Integer> sumaAreaTecnologicaDemanda(int idDemanda) {
        ArrayList<Integer> suma = new ArrayList<Integer>();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT IF(a.fkAreaTecnologicaN2 IS NULL,a.fkAreaTecnologicaN1,IF(a.fkAreaTecnologicaN3 IS NULL,(SELECT COUNT(*) FROM areatecnologican1) + a.fkAreaTecnologicaN2,(SELECT COUNT(*) FROM areatecnologican1) + (SELECT COUNT(*) FROM areatecnologican2) + a.fkAreaTecnologicaN3)) AS Suma FROM demandaareatecnologica da JOIN areatecnologica a on da.fkAreaTecnologica = a.idAreaTecnologica  WHERE da.fkDemanda = '" + idDemanda + "';";
            //String selectSQL = "SELECT IF(a.fkAreaTecnologicaN2 IS NULL,a.fkAreaTecnologicaN1,IF(a.fkAreaTecnologicaN3 IS NULL,(SELECT COUNT(*) FROM areatecnologican1) + a.fkAreaTecnologicaN2,(SELECT COUNT(*) FROM areatecnologican1) + (SELECT COUNT(*) FROM areatecnologican2) + a.fkAreaTecnologicaN3)) AS Suma FROM Necesidadtecnologica  da JOIN areatecnologica a on da.fkAreaTecnologica = a.idAreaTecnologica  WHERE da.idNecesidadTecnologica = '" + idDemanda + "';";

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                suma.add(rs.getInt("Suma"));
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public ArrayList<Integer> sumaSectorEconomicoDemanda(int idDemanda) {
        ArrayList<Integer> suma = new ArrayList<Integer>();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT IF(a.fkActividadNivel2 IS NULL,a.fkActividadNivel1,IF(a.fkActividadNivel3 IS NULL,(SELECT COUNT(*) FROM actividadeconomican1) + a.fkActividadNivel2,IF(a.fkActividadNivel4 IS NULL,(SELECT COUNT(*) FROM actividadeconomican1) + (SELECT COUNT(*) FROM actividadeconomican2) + a.fkActividadNivel3,(SELECT COUNT(*) FROM actividadeconomican1) + (SELECT COUNT(*) FROM actividadeconomican2) + (SELECT COUNT(*) FROM actividadeconomican3) + a.fkActividadNivel4))) AS Suma FROM demandasector da JOIN sectoreconomico a on da.fkSectorEconomico = a.idSectorEconomico WHERE da.fkDemanda = '" + idDemanda + "';";
            //String selectSQL = "SELECT IF(a.fkActividadEconomicaN2 IS NULL,a.fkActividadEconomicaN1,IF(a.fkActividadEconomicaN3 IS NULL,(SELECT COUNT(*) FROM actividadeconomican1) + a.fkActividadEconomicaN2,IF(a.fkActividadEconomicaN4 IS NULL,(SELECT COUNT(*) FROM actividadeconomican1) + (SELECT COUNT(*) FROM actividadeconomican2) + a.fkActividadEconomicaN3,(SELECT COUNT(*) FROM actividadeconomican1) + (SELECT COUNT(*) FROM actividadeconomican2) + (SELECT COUNT(*) FROM actividadeconomican3) + a.fkActividadEconomicaN4))) AS Suma FROM NecesidadTecnologica da JOIN sectoreconomico a on da.fkSectorEconomico = a.idSectorEconomico WHERE da.idNecesidadTecnologica = '" + idDemanda + "';";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                suma.add(rs.getInt("Suma"));
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public ArrayList<Integer> sumaActividadOferta(int idOferta) {
        ArrayList<Integer> suma = new ArrayList<Integer>();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT IF(a.fkActividadN2 IS NULL,a.fkActividadN1,IF(a.fkActividadN3 IS NULL,(SELECT COUNT(*) FROM actividadn1) + a.fkActividadN2,(SELECT COUNT(*) FROM actividadn1) + (SELECT COUNT(*) FROM actividadn2) + a.fkActividadN3)) AS Suma FROM ofertaactividad da JOIN actividad a on da.fkActividad = a.idActividad WHERE da.fkOferta = '" + idOferta + "';";
            //String selectSQL = "SELECT IF(a.fkActividadN2 IS NULL,a.fkActividadN1,IF(a.fkActividadN3 IS NULL,(SELECT COUNT(*) FROM actividadn1) + a.fkActividadN2,(SELECT COUNT(*) FROM actividadn1) + (SELECT COUNT(*) FROM actividadn2) + a.fkActividadN3)) AS Suma FROM CapacidadTecnologica da JOIN actividad a on da.fkActividad = a.idActividad WHERE da.idCapacidadTecnologica = '" + idOferta + "';";

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                suma.add(rs.getInt("Suma"));
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public ArrayList<Integer> sumaAreaCientificaOferta(int idOferta) {
        ArrayList<Integer> suma = new ArrayList<Integer>();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT IF(a.fkAreaCientificaN2 IS NULL,a.fkAreaCientificaN1,IF(a.fkAreaCientificaN3 IS NULL,(SELECT COUNT(*) FROM areacientifican1) + a.fkAreaCientificaN2,(SELECT COUNT(*) FROM areacientifican1) + (SELECT COUNT(*) FROM areacientifican2) + a.fkAreaCientificaN3)) AS Suma FROM ofertaareacientifica da JOIN areacientifica a on da.fkAreaCientifica = a.idAreaCientifica  WHERE da.fkOferta = '" + idOferta + "';";
            //String selectSQL = "SELECT IF(a.fkAreaCientificaN2 IS NULL,a.fkAreaCientificaN1,IF(a.fkAreaCientificaN3 IS NULL,(SELECT COUNT(*) FROM areacientifican1) + a.fkAreaCientificaN2,(SELECT COUNT(*) FROM areacientifican1) + (SELECT COUNT(*) FROM areacientifican2) + a.fkAreaCientificaN3)) AS Suma FROM CapacidadTecnologica da JOIN areacientifica a on da.fkAreaCientifica = a.idAreaCientifica  WHERE da.idCapacidadTecnologica = '" + idOferta + "';";


            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                suma.add(rs.getInt("Suma"));
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public ArrayList<Integer> sumaAreaTecnologicaOferta(int idOferta) {
        ArrayList<Integer> suma = new ArrayList<Integer>();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT IF(a.fkAreaTecnologicaN2 IS NULL,a.fkAreaTecnologicaN1,IF(a.fkAreaTecnologicaN3 IS NULL,(SELECT COUNT(*) FROM areatecnologican1) + a.fkAreaTecnologicaN2,(SELECT COUNT(*) FROM areatecnologican1) + (SELECT COUNT(*) FROM areatecnologican2) + a.fkAreaTecnologicaN3)) AS Suma FROM ofertaareatecnologica da JOIN areatecnologica a on da.fkActividadTecnologica = a.idAreaTecnologica  WHERE da.fkOferta = '" + idOferta + "';";
            //String selectSQL = "SELECT IF(a.fkAreaTecnologicaN2 IS NULL,a.fkAreaTecnologicaN1,IF(a.fkAreaTecnologicaN3 IS NULL,(SELECT COUNT(*) FROM areatecnologican1) + a.fkAreaTecnologicaN2,(SELECT COUNT(*) FROM areatecnologican1) + (SELECT COUNT(*) FROM areatecnologican2) + a.fkAreaTecnologicaN3)) AS Suma FROM CapaciodadTecnlogica da JOIN areatecnologica a on da.fkActividadTecnologica = a.idAreaTecnologica  WHERE da.idCapacidadTecnologica = '" + idOferta + "';";

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                suma.add(rs.getInt("Suma"));
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public ArrayList<Integer> sumaSectorEconomicoOferta(int idOferta) {
        ArrayList<Integer> suma = new ArrayList<Integer>();
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT IF(a.fkActividadNivel2 IS NULL,a.fkActividadNivel1,IF(a.fkActividadNivel3 IS NULL,(SELECT COUNT(*) FROM actividadeconomican1) + a.fkActividadNivel2,IF(a.fkActividadNivel4 IS NULL,(SELECT COUNT(*) FROM actividadeconomican1) + (SELECT COUNT(*) FROM actividadeconomican2) + a.fkActividadNivel3,(SELECT COUNT(*) FROM actividadeconomican1) + (SELECT COUNT(*) FROM actividadeconomican2) + (SELECT COUNT(*) FROM actividadeconomican3) + a.fkActividadNivel4))) AS Suma FROM ofertasector da JOIN sectoreconomico a on da.fkSectorEconomico = a.idSectorEconomico WHERE da.fkOferta = '" + idOferta + "';";
            //String selectSQL = "SELECT IF(a.fkActividadEconomicaN2 IS NULL,a.fkActividadEconomicaN1,IF(a.fkActividadEconomicaN3 IS NULL,(SELECT COUNT(*) FROM actividadeconomican1) + a.fkActiviadadEconomicaN2,IF(a.fkActividadEconomicaN4 IS NULL,(SELECT COUNT(*) FROM actividadeconomican1) + (SELECT COUNT(*) FROM actividadeconomican2) + a.fkActividadEconomicaN3,(SELECT COUNT(*) FROM actividadeconomican1) + (SELECT COUNT(*) FROM actividadeconomican2) + (SELECT COUNT(*) FROM actividadeconomican3) + a.fkActividadEconomicaN4))) AS Suma FROM CapacidadTecnologica da JOIN sectoreconomico a on da.fkSectorEconomico = a.idSectorEconomico WHERE da.idCapacidadTecnologica = '" + idOferta + "';";

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                suma.add(rs.getInt("Suma"));
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public void suma(String catalogo) {
        if (catalogo.equals("Economic_Activity")) {
            suman1 = totalAE(1);
            suman2 = totalAE(2);
            suman3 = totalAE(3);
            suman4 = totalAE(4);
        } else if (catalogo.equals("Activity")) {
            suman1 = totalA(1);
            suman2 = totalA(2);
            suman3 = totalA(3);
        } else if (catalogo.equals("TechnologicalField")) {
            suman1 = totalAT(1);
            suman2 = totalAT(2);
            suman3 = totalAT(3);
        } else if (catalogo.equals("KnowledgeField")) {
            suman1 = totalAC(1);
            suman2 = totalAC(2);
            suman3 = totalAC(3);
        }
    }

    public int totalAE(int nivel) {
        int suma = 0;

        String selectN1 = "SELECT COUNT(*) AS Suma FROM ActividadEconomicaN1";//hace un conteo de las actividadeseconomicas de nivel 1
        String selectN2 = "SELECT COUNT(*) AS Suma FROM ActividadEconomicaN2";//hace un conteo de las actividadeseconomicas de nivel 2
        String selectN3 = "SELECT COUNT(*) AS Suma FROM ActividadEconomicaN3";//hace un conteo de las actividadeseconomicas de nivel 3
        String selectN4 = "SELECT COUNT(*) AS Suma FROM ActividadEconomicaN4";//hace un conteo de las actividadeseconomicas de nivel 4

        try {

            Connection con = ConnectionPool.getConexion();

            switch (nivel) {
                case 1: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN1);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 2: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN2);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 3: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN3);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 4: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN4);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                default: {
                    break;
                }
            }

            con.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public int totalA(int nivel) {
        int suma = 0;

        String selectN1 = "SELECT COUNT(*) AS Suma FROM ActividadN1";
        String selectN2 = "SELECT COUNT(*) AS Suma FROM ActividadN2";
        String selectN3 = "SELECT COUNT(*) AS Suma FROM ActividadN3";

        try {

            Connection con = ConnectionPool.getConexion();

            switch (nivel) {
                case 1: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN1);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 2: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN2);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 3: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN3);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                default: {
                    break;
                }
            }

            con.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public int totalAC(int nivel) {
        int suma = 0;

        String selectN1 = "SELECT COUNT(*) AS Suma FROM AreaCientificaN1";
        String selectN2 = "SELECT COUNT(*) AS Suma FROM AreaCientificaN2";
        String selectN3 = "SELECT COUNT(*) AS Suma FROM AreaCientificaN3";

        try {

            Connection con = ConnectionPool.getConexion();

            switch (nivel) {
                case 1: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN1);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 2: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN2);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 3: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN3);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                default: {
                    break;
                }
            }

            con.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public int totalAT(int nivel) {
        int suma = 0;

        String selectN1 = "SELECT COUNT(*) AS Suma FROM AreaTecnologicaN1";
        String selectN2 = "SELECT COUNT(*) AS Suma FROM AreaTecnologicaN2";
        String selectN3 = "SELECT COUNT(*) AS Suma FROM AreaTecnologicaN3";

        try {

            Connection con = ConnectionPool.getConexion();

            switch (nivel) {
                case 1: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN1);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 2: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN2);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 3: {
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN3);

                    while (rs.next()) {
                        suma = rs.getInt("Suma");
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                default: {
                    break;
                }
            }

            con.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return suma;
    }

    public void conecta() {
        try {
            conn = ConnectionPool.getConexion();
        } catch (SQLException ex) {
            System.out.println("Connecion: " + ex.getMessage());
        }
    }

    public void desconecta() {
        try {
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Desconnecion: " + ex.getMessage());
        }
    }

    public List<Registro> extraeCatalogo(int nivel, int id, String catalogo) {
        List<Registro> lista = new ArrayList<Registro>();
        //para obtener datos del catalogo deseado
        String aen1 = "SELECT idActividadEconomicaN1 AS id FROM ActividadEconomicaN1 ";// se seleccionan todos los elementos del catalogo del nivel n1
        String aen2 = "SELECT idActividadEconomicaN2 AS id FROM ActividadEconomicaN2 WHERE fkActividadEconomicaN1 = " + id + " ";
        String aen3 = "SELECT idActividadEconomicaN3 AS id FROM ActividadEconomicaN3 WHERE fkActividadEconomicaN2 = " + id + " ";
        String aen4 = "SELECT idActividadEconomicaN4 AS id FROM ActividadEconomicaN4 WHERE fkActividadEconomicaN3 = " + id + " ";

        String an1 = "SELECT idActividadN1 AS id FROM ActividadN1 ";
        String an2 = "SELECT idActividadN2 AS id FROM ActividadN2 WHERE fkActividadN1 = " + id + " ";
        String an3 = "SELECT idActividadN3 AS id FROM ActividadN3 WHERE fkActividadN2 = " + id + " ";
        
        String atn1 = "SELECT idAreaTecnologicaN1 AS id FROM AreaTecnologicaN1 ";
        String atn2 = "SELECT idAreaTecnologicaN2 AS id FROM AreaTecnologicaN2 WHERE fkAreaTecnologicaN1 = " + id + " ";
        String atn3 = "SELECT idAreaTecnologicaN3 AS id FROM AreaTecnologicaN3 WHERE fkAreaTecnologicaN2 = " + id + " ";

        String acn1 = "SELECT idAreaCientificaN1 AS id FROM AreaCientificaN1 ";
        String acn2 = "SELECT idAreaCientificaN2 AS id FROM AreaCientificaN2 WHERE fkAreaCientificaN1 = " + id + " ";
        String acn3 = "SELECT idAreaCientificaN3 AS id FROM AreaCientificaN3 WHERE fkAreaCientificaN2 = " + id + " ";
        /*FALTAN LAS TABLAS*/
        String recursos = "SELECT idTipoRecursoTecnologico AS id FROM TipoRecursoTecnologico ";
        //String recursos = "SELECT idTipoInstalacion AS id FROM TipoInstalacion ";
        String instalacion = "SELECT idTipoInstalacion AS id FROM TipoInstalacion ";
        //String instalacion = "SELECT idTipoInfraestructura AS id FROM TipoInfraestructura ";
        /*FALTAN LAS TABLAS de personal*/
        String selectN1 = "";
        String selectN2 = "";
        String selectN3 = "";
        String selectN4 = "";

        if (catalogo.equals("Economic_Activity")) {
            selectN1 = aen1;
            selectN2 = aen2;
            selectN3 = aen3;
            selectN4 = aen4;
        } else if (catalogo.equals("Activity")) {
            selectN1 = an1;
            selectN2 = an2;
            selectN3 = an3;
        } else if (catalogo.equals("TechnologicalField")) {
            selectN1 = atn1;
            selectN2 = atn2;
            selectN3 = atn3;
        } else if (catalogo.equals("KnowledgeField")) {
            selectN1 = acn1;
            selectN2 = acn2;
            selectN3 = acn3;
        } else if (catalogo.equals("Resource")) {
            selectN1 = recursos;
        } else if (catalogo.equals("FuntionalPlace")) {
            selectN1 = instalacion;
        }

        try {

            switch (nivel) {
                case 0: {
//                    System.out.println("Caso 0");
                    Registro r = new Registro(null, null, null);
                    r.nodo = catalogo + "_0";
                    lista.add(r);
                    break;
                }
                case 1: {
//                    System.out.println("Caso 1");
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN1);

                    while (rs.next()) {
                        Registro r = new Registro(null, null, null);
                        r.idBd = rs.getInt("id");
                        r.nodo = catalogo + "_" + r.idBd;
                        lista.add(r);
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 2: {
//                    System.out.println("Caso 2");
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN2);

                    while (rs.next()) {
                        Registro r = new Registro(null, null, null);
                        r.idBd = rs.getInt("id");//<------------
                        int suma = 0;
                        suma = suman1 + r.idBd;
                        r.nodo = catalogo + "_" + suma;
                        lista.add(r);//<------------------------
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 3: {
//                    System.out.println("Caso 3");
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN3);

                    while (rs.next()) {
                        Registro r = new Registro(null, null, null);
                        r.idBd = rs.getInt("id");
                        int suma = 0;
                        suma = suman1 + suman2 + r.idBd;
                        r.nodo = catalogo + "_" + suma;
                        lista.add(r);
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                case 4: {
//                    System.out.println("Caso 4");
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery(selectN4);

                    while (rs.next()) {
                        Registro r = new Registro(null, null, null);
                        r.idBd = rs.getInt("id");
                        int suma = 0;
                        suma = suman1 + suman2 + suman3 + r.idBd;
                        r.nodo = catalogo + "_" + suma;
                        lista.add(r);
                    }

                    rs.close();
                    stmt.close();
                    break;
                }
                default: {
                    break;
                }
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return lista;
    }
    
    public float extraeUmbralTd(int idDemanda) {
        float umbral = 0.0f;
        try {
            Connection con = ConnectionPool.getConexion();// conexion a la base
            // consulta para obtener los factores de una demanda en especifico
            String selectSQL = "SELECT U.UmbralPeso FROM FactoresPreferencia FP JOIN  FactorUmbralPeso U ON U.idFactorUmbralPeso = FP.fkFactorUmbralPeso WHERE FP.fkDemanda = '" + idDemanda + "';";

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            // lista para almacenar los factores

            while (rs.next()) {
                umbral = rs.getFloat("UmbralPeso");
            }

            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return umbral;
    }

}



//SELECT da.fkDemanda,a.*,IF(a.fkActividadNivel2 IS NULL,a.fkActividadNivel1,IF(a.fkActividadNivel3 IS NULL,(SELECT COUNT(*) FROM actividadeconomican1) + a.fkActividadNivel2,IF(a.fkActividadNivel4 IS NULL,(SELECT COUNT(*) FROM actividadeconomican1) + (SELECT COUNT(*) FROM actividadeconomican2) + a.fkActividadNivel3,(SELECT COUNT(*) FROM actividadeconomican1) + (SELECT COUNT(*) FROM actividadeconomican2) + (SELECT COUNT(*) FROM actividadeconomican3) + a.fkActividadNivel4))) AS Suma FROM demandasector da JOIN sectoreconomico a on da.fkSectorEconomico = a.idSectorEconomico
