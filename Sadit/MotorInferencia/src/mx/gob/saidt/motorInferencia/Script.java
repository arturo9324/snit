/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia;

import java.io.FileWriter;
import java.io.PrintWriter;
import mx.gob.saidt.util.ObtenerRutaDirectorios;

/**
 *
 * @author erick-mirsha
 */
public class Script {

    public boolean generarScript() {
        ObtenerRutaDirectorios r = new ObtenerRutaDirectorios();
        String tipoSo = r.obtenerSO();
        System.out.println("SO: " + tipoSo);

        if (!tipoSo.isEmpty()) {
            if (tipoSo.equals("linux")) {
                return generarScriptSh();
            } else if (tipoSo.equals("windows")) {
                return generarScriptBatch();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean generarScriptSh() {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            ObtenerRutaDirectorios r = new ObtenerRutaDirectorios();
            String pathMotor = r.obtenerRutaOntologias();
            String pathFreeling = r.obtenerRutaFreeling();

            System.out.println("PathMotor: " + pathMotor);
            System.out.println("PathFreeling: " + pathFreeling);

            if (!pathMotor.isEmpty() && !pathFreeling.isEmpty()) {
                fichero = new FileWriter(pathMotor + "script.sh");
                pw = new PrintWriter(fichero);

                pw.println("#!/bin/bash");
                pw.println("# -*- ENCODING: UTF-8 -*-");
                pw.println("echo 'Iniciando analisis freeling...'");
                pw.println(pathFreeling + "analyze -f es.cfg --flush --outf tagged <" + pathMotor + "DemandasOfertas.txt > " + pathMotor + "DemandasOfertasAnalizadas.txt");
                pw.println("echo 'Finalizando analisis...'");
                pw.println("exit");
                pw.close();
            } else {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public boolean generarScriptBatch() {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            ObtenerRutaDirectorios r = new ObtenerRutaDirectorios();
            String pathMotor = r.obtenerRutaOntologias();
            String pathFreeling = r.obtenerRutaFreeling();

            System.out.println("PathMotor: " + pathMotor);
            System.out.println("PathFreeling: " + pathFreeling);

            if (!pathMotor.isEmpty() && !pathFreeling.isEmpty()) {
                fichero = new FileWriter(pathMotor + "script.bat");
                pw = new PrintWriter(fichero);

                pw.println("@Echo Off");
                pw.println("echo Iniciando analisis freeling...");
                pw.println("Set OLDPATH=%PATH%");
                pw.println("Set PATH=" + pathFreeling + "bin;%PATH%");
                pw.println("Set FREELINGSHARE=" + pathFreeling + "data");
                pw.println(pathFreeling+"bin/analyzer.exe -f %FREELINGSHARE%/config/es.cfg --flush --outf tagged <" + pathMotor + "DemandasOfertas.txt > " + pathMotor + "DemandasOfertasAnalizadas.txt");
                pw.println("echo Finalizando analisis...");
                pw.println("exit");
                pw.close();
            } else {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
