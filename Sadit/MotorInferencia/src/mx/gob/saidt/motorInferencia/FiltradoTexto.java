/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.motorInferencia;

import mx.gob.saidt.motorInferencia.dao.CalculaCatalogoDao;
import mx.gob.saidt.motorInferencia.dao.DaoFiltrados;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.gob.saidt.util.ObtenerRutaDirectorios;

/**
 *
 * @author
 */
public class FiltradoTexto {

    static void DemandaOferaTexto(List recursosAsociados, String path) throws UnsupportedEncodingException, IOException {
        Writer out = null;

        try {
            ObtenerRutaDirectorios r = new ObtenerRutaDirectorios();
            String tipoSo = r.obtenerSO();
            System.out.println("SO: " + tipoSo);
            String encode = "";

            if (!tipoSo.isEmpty()) {
                if (tipoSo.equals("windows")) {
                    encode = "windows-1252";
                }
            }

            if(!encode.equals("")){
                out = new OutputStreamWriter(new FileOutputStream(((path + "DemandasOfertas.txt").equals("")) ? (path + "DemandasOfertas.txt") : (path + "DemandasOfertas.txt")), encode);
            } else {
                out = new OutputStreamWriter(new FileOutputStream(((path + "DemandasOfertas.txt").equals("")) ? (path + "DemandasOfertas.txt") : (path + "DemandasOfertas.txt")));
            }
            //escribe en el archivo las demandas con sus ofertas separadas por (^)oferta  (~)demanda
            int idDemanda = 0;
            int idOferta = 0;
            DaoFiltrados sql = new DaoFiltrados();
            for (int i = 0; i < recursosAsociados.size(); i++) {

                List deof = (List) recursosAsociados.get(i);
                idDemanda = Integer.parseInt(deof.get(0) + "");
                List demanda = sql.listaDemandas(idDemanda);
                out.write("^\n");
                out.write("idDEMANDA: " + demanda.get(0) + "" + "\n");
                out.write("TITULO: " + demanda.get(1) + "" + "\n");
                out.write("RESUMEN: " + demanda.get(2) + "" + "\n");
                out.write("DESCRIPCION: " + demanda.get(3) + "" + "\n");
                out.write("KEYWORDS: " + demanda.get(4) + "" + "\n\n");
                for (int j = 1; j < deof.size(); j++) {
                    List ofertaSimilitud = (List) deof.get(j);
                    idOferta = Integer.parseInt(ofertaSimilitud.get(0) + "");
                    List ofertas = sql.listaOfertas(idOferta);
                    List ofertasRecurso = sql.ofertaRecursoTec(idOferta);

                    List ofertaInfra = sql.ofertaInfra(idOferta);
                    try {

                        out.write("~\nidOFERTA: " + ofertas.get(0) + "" + "\n");
                        if (ofertasRecurso.size() > 0) {
                            out.write("TITULO: " + ofertas.get(1) + " " + ofertasRecurso.get(0) + "\n");
                            out.write("DESCRIPCION: " + ofertas.get(3) + " " + ofertasRecurso.get(1) + "\n");
                        } else if (ofertaInfra.size() > 0) {
                            out.write("TITULO: " + ofertas.get(1) + " " + ofertaInfra.get(0) + "\n");
                            out.write("DESCRIPCION: " + ofertas.get(3) + " " + ofertaInfra.get(1) + "\n");

                        } else {
                            out.write("TITULO: " + ofertas.get(1) + "" + "\n");
                            out.write("DESCRIPCION: " + ofertas.get(3) + "" + "\n");
                        }

                        out.write("RESUMEN: " + ofertas.get(2) + "" + "\n");
                        out.write("KEYWORDS: " + ofertas.get(4) + "" + "\n\n");
                    } catch (Exception e) {
                    }
                }
            }
            out.write("^\n");
            out.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            Logger.getLogger(FiltradoTexto.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public List tfIdf(String path) {
        int numPalabra = 0;
        List palabrasGeneral = new ArrayList();
        try {
            FileInputStream fstream = new FileInputStream(path + "DemandasOfertasAnalizadas.txt");
            DataInputStream entrada = new DataInputStream(fstream);
            BufferedReader buffer = new BufferedReader(new InputStreamReader(entrada));
            //palabras en los textos para identificar las partes del parrafo
            String[] paltex = {"idDEMANDA", "TITULO", "RESUMEN", "DESCRIPCION", "KEYWORDS", "idOFERTA"};//keywords para el taxto 
            List palPa = Arrays.asList(paltex);
            //palabras irrelevantes
            String[] codigos = {"P00CN000", "SPS00", "DA0MS0", "DA0FS0", "DA0MP0", "CS", "PR0CN000", "CC", "DI0FP0", "DP3CS0", "Fd", "Fc",
                "DI0MS0", "Fp", "Fpa", "Fpt", "PP3FSA00"};//palabras a ignorar
            List palabraNoUtilizable = Arrays.asList(codigos);

            String linea;
            // Leer el archivo linea por linea

            List palabrasDemanda = new ArrayList();
            List palabrasOferta = new ArrayList();
            List demanda = new ArrayList();
            List ofertas = new ArrayList();
            boolean leyendoDemanda = false;
            boolean leyendoOferta = false;
            while ((linea = buffer.readLine()) != null) {

                // Imprimimos la línea por pantalla
                String lineaArray[] = linea.split(" ");
                if (lineaArray.length > 1) {
//                    System.out.println(linea);
                    //elimina las palabras irrelevantes y las palabras que identifican cada conjunto del parrafo
                    if (!palabraNoUtilizable.contains(lineaArray[2]) && !palPa.contains(lineaArray[0])) {
                        if (linea.contains("^")) {
                            //Comienza una nueva demanda
//                            System.out.println("Comienza una nueva demanda");
                            leyendoDemanda = true;
                            if (palabrasDemanda.size() > 0) {
                                demanda.add(palabrasDemanda);
//                                System.out.println("Se agregó una demanda");
                                ofertas.add(palabrasOferta);
//                                System.out.println("Se agregarón las ofertas");
                                demanda.add(ofertas);
                                palabrasGeneral.add(demanda);
                            }
                            palabrasDemanda = new ArrayList();
                            palabrasOferta = new ArrayList();
                            ofertas = new ArrayList();
                            demanda = new ArrayList();
                            leyendoOferta = false;
                            continue;

                        }
                        if (linea.contains("~")) {
                            //Comienza una oferta
//                            System.out.println("Comienza una oferta");
                            leyendoOferta = true;
                            leyendoDemanda = false;
                            if (palabrasOferta.size() > 0) {
                                ofertas.add(palabrasOferta);
                                palabrasOferta = new ArrayList();
                                //leyendoOferta = false;
//                                System.out.println("Se agregó una oferta");
                            }
                            continue;
                        }

                        if (leyendoDemanda) {
                            palabrasDemanda.add(lineaArray[1]);
                        }

                        if (leyendoOferta) {
                            palabrasOferta.add(lineaArray[1]);
                        }
                    }

//                    System.out.println("------------------------------");
                }

            }

            System.out.println("Lista Palabras" + palabrasGeneral);

            //Obtener conteo de apariciones de palabra en cada texto
            for (int i = 0; i < palabrasGeneral.size(); i++) {
                List demandasOfertas = (List) palabrasGeneral.get(i);
                List demand = (List) demandasOfertas.get(0);
                //Recorrer palabras de demanda y llenar lista auxiliar (demandCount) con total de apariciones por palabra
                List demandCount = new ArrayList();
                for (int j = 0; j < demand.size(); j++) {
                    List pal = new ArrayList();
                    pal.add(demand.get(j));
                    int cont = 0;
                    for (int k = 0; k < demand.size(); k++) {
                        if (demand.get(j).equals(demand.get(k))) {
                            cont++;
                        }
                    }
                    pal.add(cont);
                    demandCount.add(pal);
                }
                demandasOfertas.set(0, demandCount);
                palabrasGeneral.set(i, demandasOfertas);

                List ofers = (List) demandasOfertas.get(1);
                if (ofers.size() > 0) {
                    for (int j = 0; j < ofers.size(); j++) {
                        List ofer = (List) ofers.get(j);
                        //Recorrer palabras de oferta y llenar lista auxiliar (ofertaCount) con total de apariciones por palabra
                        List ofertaCount = new ArrayList();
                        for (int k = 0; k < ofer.size(); k++) {
                            List pal = new ArrayList();
                            pal.add(ofer.get(k));
                            int cont = 0;
                            for (int l = 0; l < ofer.size(); l++) {
                                if (ofer.get(k).equals(ofer.get(l))) {
                                    cont++;
                                }
                            }
                            pal.add(cont);
                            ofertaCount.add(pal);
                        }
                        ofers.set(j, ofertaCount);
                        demandasOfertas.set(1, ofers);
                        palabrasGeneral.set(i, demandasOfertas);
                    }
                }
            }

            //Valor TF
            for (int i = 0; i < palabrasGeneral.size(); i++) {
                List demandaOferta = (List) palabrasGeneral.get(i);
                List demand = (List) demandaOferta.get(0);
                int maxCount = 0;
                //Obtener el maximo de las palabras de una demanda
                for (int j = 0; j < demand.size(); j++) {
                    List pal = (List) demand.get(j);
                    if ((Integer) pal.get(1) > maxCount) {
                        maxCount = (Integer) pal.get(1);
                    }
                }

                //Dividir repeticiones de cada palabra entre el numero maximo de repeticiones de una palabra (Demandas)
                for (int j = 0; j < demand.size(); j++) {
                    List pal = (List) demand.get(j);
                    float tf = Float.parseFloat(pal.get(1).toString()) / maxCount;
                    pal.add(tf);
                }

                List ofers = (List) demandaOferta.get(1);
                int maxCountOf = 0;
                //Obtener el maximo de las palabras de una oferta
                for (int j = 0; j < ofers.size(); j++) {
                    List ofer = (List) ofers.get(j);
                    for (int k = 0; k < ofer.size(); k++) {
                        List pal = (List) ofer.get(k);
                        if ((Integer) pal.get(1) > maxCountOf) {
                            maxCountOf = (Integer) pal.get(1);
                        }
                    }

                }

                //Dividir repeticiones de cada palabra entre el numero maximo de repeticiones de una palabra (Ofertas)
                for (int j = 0; j < ofers.size(); j++) {
                    List ofer = (List) ofers.get(j);
                    for (int k = 0; k < ofer.size(); k++) {
                        List pal = (List) ofer.get(k);
                        //Calcular y agregar valor tf
                        float tf = Float.parseFloat(pal.get(1).toString()) / maxCountOf;
                        pal.add(tf);
                    }
                }
            }

            //Obtener IDF 
            for (int i = 0; i < palabrasGeneral.size(); i++) {
                List ofertaDemanda = (List) palabrasGeneral.get(i);
                List ofers = (List) ofertaDemanda.get(1);
                if (ofers.size() > 0) {
                    for (int j = 0; j < ofers.size(); j++) {
                        List ofer = (List) ofers.get(j);
                        for (int k = 0; k < ofer.size(); k++) {
                            List pal = (List) ofer.get(k);
                            int contPalOfer = 0;
                            for (int l = 0; l < ofers.size(); l++) {
                                List ofer2 = (List) ofers.get(l);
                                for (int m = 0; m < ofer2.size(); m++) {
                                    List pal2 = (List) ofer2.get(m);
                                    if (pal.get(0).equals(pal2.get(0))) {
                                        contPalOfer++;
                                        break;
                                    }
                                }
                            }
                            //Calcular y agregar valor IDF a palabra
                            pal.add(ofers.size() / contPalOfer);
                            //Agregar valor de la multiplicación de TF * IDF
                            pal.add(Float.parseFloat(pal.get(2).toString()) * Float.parseFloat(pal.get(3).toString()));
                        }
                    }
                }
            }

            //Mostrar contenido de lista
            /*
             * == Demandas ==
             * Posicion 0 = Palabra
             * Posicion 1 = Número de veces que aparece en el parrafo
             * Posicion 2 = (TF) Resultado de divicion entre veces que aparece la palabra y el numero que mas veces aparece una palabra
             * 
             * == Ofertas ==
             * Posicion 0 = Palabra
             * Posicion 1 = Número de veces que aparece en el parrafo
             * Posicion 2 = (TF) Resultado de divicion entre veces que aparece la palabra y el numero que mas veces aparece una palabra
             * posicion 3 = (IDF) Resultado de divicion entre total de ofertas y numero de ofertas en la que aparece la palabra
             * posicion 4 = (TF * IDF) 
             */
            for (int i = 0; i < palabrasGeneral.size(); i++) {
                List demandasOfertas = (List) palabrasGeneral.get(i);
                List demand = (List) demandasOfertas.get(0);
//                System.out.println("Demanda: " + demand);
                List ofers = (List) demandasOfertas.get(1);
                if (ofers.size() > 0) {
                    for (int j = 0; j < ofers.size(); j++) {
                        List ofer = (List) ofers.get(j);
//                        System.out.println("\tOferta: " + ofer);
                    }
                }
            }
            // Cerramos el archivo
            entrada.close();

        } catch (Exception e) { //Catch de excepciones
            e.printStackTrace();
        }
        return palabrasGeneral;
    }

    public List coseno(List palabrasGeneral) {
        System.out.println("\n=========Coseno=========\n");
        for (int i = 0; i < palabrasGeneral.size(); i++) {
            List demandasOfertas = (List) palabrasGeneral.get(i);
            List demand = (List) demandasOfertas.get(0);
//            System.out.println("Demanda: " + demand);
            //Conjunto A
            List conjuntoA = new ArrayList();
            for (int j = 0; j < demand.size(); j++) {
                List palabra = (List) demand.get(j);
                List palConjuntoA = new ArrayList();
                palConjuntoA.add(palabra.get(0));
                palConjuntoA.add(palabra.get(2));
                conjuntoA.add(palConjuntoA);
            }

            //Conjunto B
            List conjuntoB = new ArrayList();
            List ofers = (List) demandasOfertas.get(1);
            if (ofers.size() > 0) {
                for (int j = 0; j < ofers.size(); j++) {
                    List ofer = (List) ofers.get(j);
                    if (ofer.size() > 0) {
//                        System.out.println("\tOferta: " + ofer);
                        for (int k = 0; k < ofer.size(); k++) {
                            List palabra = (List) ofer.get(k);
                            List palConjuntoB = new ArrayList();
                            palConjuntoB.add(palabra.get(0));
                            palConjuntoB.add(palabra.get(4));
                            conjuntoB.add(palConjuntoB);
                        }

                        //AUB
                        List aUnionB = new ArrayList();
                        aUnionB.addAll(conjuntoA);
                        for (int k = 0; k < conjuntoB.size(); k++) {
                            List palabra = (List) conjuntoB.get(k);
                            //Buscar palabra existente en conjuntoA
                            boolean existe = false;
                            for (int l = 0; l < conjuntoA.size(); l++) {
                                List pal = (List) conjuntoA.get(l);
                                if (pal.get(0).equals(palabra.get(0))) {
                                    existe = true;
                                    break;
                                }
                            }
                            if (!existe) {
                                aUnionB.add(palabra);
                            }

                        }

                        //AUB A
                        List aUnionBA = new ArrayList();
                        for (int k = 0; k < aUnionB.size(); k++) {
                            List palabra = (List) aUnionB.get(k);
                            List palabraAUBA = new ArrayList();
                            palabraAUBA.add(palabra.get(0));
                            boolean existe = false;
                            for (int l = 0; l < conjuntoA.size(); l++) {
                                List pal = (List) conjuntoA.get(l);
                                if (pal.get(0).equals(palabra.get(0))) {
                                    //Existe en conjunto A
                                    existe = true;
                                    palabraAUBA.add(pal.get(1));
                                    break;
                                }
                            }
                            if (!existe) {
                                palabraAUBA.add(0.0);
                            }
                            aUnionBA.add(palabraAUBA);
                        }

                        //AUB B
                        List aUnionBB = new ArrayList();
                        for (int k = 0; k < aUnionB.size(); k++) {
                            List palabra = (List) aUnionB.get(k);
                            List palabraAUBB = new ArrayList();
                            palabraAUBB.add(palabra.get(0));
                            boolean existe = false;
                            for (int l = 0; l < conjuntoB.size(); l++) {
                                List pal = (List) conjuntoB.get(l);
                                if (pal.get(0).equals(palabra.get(0))) {
                                    //Existe en conjunto A
                                    existe = true;
                                    palabraAUBB.add(pal.get(1));
                                    break;
                                }
                            }
                            if (!existe) {
                                palabraAUBB.add(0.0);
                            }
                            aUnionBB.add(palabraAUBB);
                        }

                        //A*B
                        List aXb = new ArrayList();
                        float sumaAxB = 0.0f;
                        for (int k = 0; k < aUnionBA.size(); k++) {
                            List palabraA = (List) aUnionBA.get(k);
                            List palabraB = (List) aUnionBB.get(k);
                            List palabraAXB = new ArrayList();
                            palabraAXB.add(palabraA.get(0));
                            palabraAXB.add(Float.parseFloat(palabraA.get(1) + "") * Float.parseFloat(palabraB.get(1) + ""));
                            aXb.add(palabraAXB);
                            sumaAxB += Float.parseFloat(palabraA.get(1) + "") * Float.parseFloat(palabraB.get(1) + "");
                        }

                        //A al cuadrado
                        List aCuadrado = new ArrayList();
                        float sumaACuadrado = 0.0f;
                        for (int k = 0; k < aUnionBA.size(); k++) {
                            List palabra = (List) aUnionBA.get(k);
                            List palabraACuadrado = new ArrayList();
                            palabraACuadrado.add(palabra.get(0));
                            palabraACuadrado.add(Float.parseFloat(palabra.get(1) + "") * Float.parseFloat(palabra.get(1) + ""));
                            aCuadrado.add(palabraACuadrado);
                            sumaACuadrado += Float.parseFloat(palabra.get(1) + "") * Float.parseFloat(palabra.get(1) + "");
                        }

                        //B al cuadrado
                        List bCuadrado = new ArrayList();
                        float sumaBCuadrado = 0.0f;
                        for (int k = 0; k < aUnionBB.size(); k++) {
                            List palabra = (List) aUnionBB.get(k);
                            List palabraBCuadrado = new ArrayList();
                            palabraBCuadrado.add(palabra.get(0));
                            palabraBCuadrado.add(Float.parseFloat(palabra.get(1) + "") * Float.parseFloat(palabra.get(1) + ""));
                            bCuadrado.add(palabraBCuadrado);
                            sumaBCuadrado += Float.parseFloat(palabra.get(1) + "") * Float.parseFloat(palabra.get(1) + "");
                        }

                        //Coseno
                        double coseno = sumaAxB / Math.sqrt(Double.parseDouble(sumaACuadrado + "") * Double.parseDouble(sumaBCuadrado + ""));
                        ofer.add(coseno);

//                        System.out.println("\tConjunto A: " + conjuntoA);
//                        System.out.println("\tConjunto B: " + conjuntoB);
//                        System.out.println("\tAUB: " + aUnionB);
//                        System.out.println("\tAUBA: " + aUnionBA);
//                        System.out.println("\tAUBB: " + aUnionBB);
//                        System.out.println("\tAXB: " + aXb);
//                        System.out.println("\tSuma AXB: " + sumaAxB);
//                        System.out.println("\tACuadrado: " + aCuadrado);
//                        System.out.println("\tSuma ACuadrado: " + sumaACuadrado);
//                        System.out.println("\tBCuadrado: " + bCuadrado);
//                        System.out.println("\tSuma BCuadrado: " + sumaBCuadrado);
//                        System.out.println("\tCoseno: " + coseno);
//                        System.out.println("\tOferta: " + ofer);
                        //System.out.println("\t-------------------------------------------------------");
                    }

                }
            }
        }
        return palabrasGeneral;
    }

    public List sumarCosenoOferta(List recursosAsociados, List listaPalabras) {
        System.out.println("==== Suma Cos Ofertas ====");

        CalculaCatalogoDao ccd = new CalculaCatalogoDao();
        List filtroTexto = new ArrayList();
        for (int i = 0; i < listaPalabras.size(); i++) {
            List demandaOfertas = (List) listaPalabras.get(i);
            List demand = (List) demandaOfertas.get(0);
            List ofers = (List) demandaOfertas.get(1);
//            System.out.println("Demanda: "+demand);
//            System.out.println("Ofertas: "+ofers);

            List mainDemand = new ArrayList();

            List recurso = (List) recursosAsociados.get(i);
            mainDemand.add(recurso.get(0));

            List mainOfer = new ArrayList();

            if (ofers.size() > 0) {
                for (int j = 0; j < ofers.size(); j++) {
                    List ofer = (List) ofers.get(j);
                    if (ofer.size() > 0) {
//                        System.out.println("Oferta: "+ofer);
                        List idOfer = (List) ofer.get(0);
//                        System.out.println("IdOfer: "+idOfer);
                        double cos = (Double) ofer.get(ofer.size() - 1);
//                        System.out.println("Cos: " + ofer.get(ofer.size() - 1));

//                        System.out.println("Recurso: "+recurso);
                        for (int k = 1; k < recurso.size(); k++) {
                            List oferta = (List) recurso.get(k);
//                            System.out.println("OfertaRecurso: "+oferta);
                            int idOf1 = (Integer) oferta.get(0);
                            int idOf2 = Integer.parseInt(idOfer.get(0) + "");
                            //System.out.println("Comparacion: " + idOf1 + " == " + idOf2);
                            if (idOf1 == idOf2) {
//                                System.out.println("Suma: "+Double.parseDouble(oferta.get(1)+"")+" + "+cos);
                                oferta.set(1, (Double.parseDouble(oferta.get(1) + "") + cos));
                                mainOfer.add(oferta);
                                break;
                            }
                        }
                    }

                }

            }
            mainDemand.add(mainOfer);
            //System.out.println("MainDemand: "+mainDemand);
            filtroTexto.add(mainDemand);

        }
        //filtroTexto = recursosAsociados;
        return filtroTexto;
    }

    public List eliminarUmbral(List listaPalabras) {
        System.out.println("==== Descartar ofertas ====");
        CalculaCatalogoDao ccd = new CalculaCatalogoDao();
        List filtroTexto = new ArrayList();
        for (int i = 0; i < listaPalabras.size(); i++) {
            List demandaOfertas = (List) listaPalabras.get(i);
            List demand = (List) demandaOfertas.get(0);
            List ofers = (List) demandaOfertas.get(1);
//            System.out.println("Ofers: " + ofers);
            List mainDemand = (List) demand.get(0);
            int idDemanda = Integer.parseInt(mainDemand.get(0).toString());
//            System.out.println("Demanda: " + idDemanda);
//            System.out.println(demand);
            float umbral = ccd.extraeUmbralTd(idDemanda);
//            System.out.println("Umbral: " + umbral);
            if (ofers.size() > 0) {
                for (int j = 0; j < ofers.size(); j++) {
                    List ofer = (List) ofers.get(j);
                    if (ofer.size() > 0) {
                        List idOfer = (List) ofer.get(0);
//                        System.out.println("IdOfer: "+idOfer);
                        double cos = (Double) ofer.get(ofer.size() - 1);
//                        System.out.println("Cos: " + ofer.get(ofer.size() - 1));
//                        System.out.println("Oferta (Id: " + idOfer + ", Cos: " + cos + "): " + ofer);
                        boolean ok = false;
                        if (cos < umbral) {
                            ok = true;
                        }
//                        System.out.println("Eliminar oferta? (" + cos + " < " + umbral + ") " + ok);
                        if (ok) {
                            ofers.remove(j);
                            j--;
                        }

                    }
                }
            }
            //System.out.println("Demanda: " + idDemanda + ", Ofertas: " + ofers.size());
        }
        filtroTexto = listaPalabras;
        return filtroTexto;
    }

    public List formatoLista(List listaPalabras) {
        System.out.println("==== Formatear lista coseno ====");
        CalculaCatalogoDao ccd = new CalculaCatalogoDao();
        List filtroTexto = new ArrayList();
        for (int i = 0; i < listaPalabras.size(); i++) {
            List demandaOfertaCoseno = new ArrayList();
            List demandaOfertas = (List) listaPalabras.get(i);
            List demand = (List) demandaOfertas.get(0);
            List ofers = (List) demandaOfertas.get(1);
//            System.out.println("Ofers: " + ofers);
            List mainDemand = (List) demand.get(0);
            int idDemanda = Integer.parseInt(mainDemand.get(0).toString());
            float umbral = ccd.extraeUmbralTd(idDemanda);
            List ofertasCoseno = new ArrayList();
            if (ofers.size() > 0) {
                for (int j = 0; j < ofers.size(); j++) {
                    List ofer = (List) ofers.get(j);
                    if (ofer.size() > 0) {
                        List idOfer = (List) ofer.get(0);
//                        System.out.println("IdOfer: "+idOfer);
                        double cos = (Double) ofer.get(ofer.size() - 1);
//                        System.out.println("Cos: " + ofer.get(ofer.size() - 1));
                        ofertasCoseno.add(idOfer);
                        ofertasCoseno.add(cos);
                    }
                }
            }
            demandaOfertaCoseno.add(idDemanda);
            demandaOfertaCoseno.add(ofertasCoseno);
            filtroTexto.add(demandaOfertaCoseno);
        }
        return filtroTexto;
    }

    public void ejecutarFreeling() {

        try {
            ObtenerRutaDirectorios r = new ObtenerRutaDirectorios();
            String pathMotor = r.obtenerRutaOntologias();
            String tipoSo = r.obtenerSO();

            System.out.println("PathMotor: " + pathMotor);
            System.out.println("SO: " + tipoSo);

            System.out.println(pathMotor);

            String exec = "";
            if (!pathMotor.isEmpty() && !tipoSo.isEmpty()) {

                if (tipoSo.equals("linux")) {
                    exec = "sh " + pathMotor + "script.sh";
                } else if (tipoSo.equals("windows")) {
                    exec = "cmd.exe /k " + pathMotor + "script.bat";
                }
                System.out.println("Comando: " + exec);
                String s = null;
                Process p = Runtime.getRuntime().exec(exec);
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

                BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

                while ((s = stdInput.readLine()) != null) {
                    System.out.println("s1 " + s);
                }

                while ((s = stdError.readLine()) != null) {
                    System.out.println("s2 " + s);
                }

            }

            //System.exit(0);
        } catch (Exception e) {
            System.out.println("Excepción: " + e.getMessage() + " " + e.getCause());
            e.printStackTrace();
            System.exit(-1);

        }
    }

}
