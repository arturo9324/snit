/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.saidt.util;

import java.util.ResourceBundle;

/**
 *
 * @author LM (Luis-Maya)
 */
public class ObtenerRutaDirectorios {

    private ResourceBundle propiedades;

    private String extraerParametroRuta(String parametro) {
        String ruta = "";
        propiedades = ResourceBundle.getBundle("saidt");//regresa el archivo saidt.properties
        if (propiedades.containsKey(parametro)) {//verifica que contencga la llave que se recibe
            ruta = propiedades.getString(parametro);//regresa la ruta del archivo qu se desea
        }
        return ruta;
    }

    public String obtenerRutaImagenes() {
        return extraerParametroRuta("images");
    }

    public String obtenerRutaPDFs() {
        return extraerParametroRuta("pdfs");
    }

    public String obtenerRutaReportes() {
        return extraerParametroRuta("reports");
    }
    
    public String obtenerRutaOntologias() {
        //Método que obtiene todo la sección de motorIferencia de Saidt.properties
        return extraerParametroRuta("motorInferencia");//ejecutar la extracción de la ruta
    }

    public String obtenerRutaFreeling() {
        return extraerParametroRuta("freeling");
    }
    
    public String obtenerSO() {
        return extraerParametroRuta("so");
    }
   /* public static void main(String args[]) {
        ObtenerRutaDirectorios rutas = new ObtenerRutaDirectorios();
        if (!rutas.obtenerRutaImagenes().isEmpty()) {
            System.out.println("La ruta para almacenar imagenes es: " + rutas.obtenerRutaImagenes());
        } else {
            System.out.println("No hay una ruta para almacenar las imagenes.");
        }

        if (!rutas.obtenerRutaPDFs().isEmpty()) {
            System.out.println("La ruta para almacenar PDFs es: " + rutas.obtenerRutaImagenes());
        } else {
            System.out.println("No hay una ruta para almacenar las PDFs.");
        }
        if (!rutas.obtenerRutaReportes().isEmpty()) {
            System.out.println("La ruta para almacenar reportes es: " + rutas.obtenerRutaImagenes());
        } else {
            System.out.println("No hay una ruta para almacenar los reportes.");
        }
    }*/
}
